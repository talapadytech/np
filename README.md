# Neon Pinball (Based on Vector Pinball)

Neon Pinball is a pinball game for Android devices.
It is released under version 3 of the GPL; see [COPYING](COPYING.txt) for the license text.

The graphics are deliberately simple; currently everything is drawn with lines and circles.
The focus is on gameplay and accurate physics.
It uses the libgdx Java wrapper for the Box2D physics engine.
Thanks to Peter Drescher for the sound effects; see [his article on creating them](https://www.twittering.com/webarchive_articles/FMOD%20for%20Android%20-%20O'Reilly%20Broadcast.html).

The GitHub project page is: [github.com/dozingcat/Vector-Pinball/](https://github.com/dozingcat/Vector-Pinball/).
See [devnotes.txt](devnotes.txt) for an overview of the code layout.

There is a very experimental table editor at [github.com/dozingcat/Vector-Pinball-Editor/](https://github.com/dozingcat/Vector-Pinball-Editor/)

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Vector Pinball includes [the libgdx library by Bad Logic Games](http://libgdx.badlogicgames.com/).
libgdx is used under [the terms of Version 2.0 of the Apache License](https://www.apache.org/licenses/LICENSE-2.0).

Sound, music, & audio code by [pdx of Twittering Machine](http://www.twittering.com).
