package com.talapady.neon.pinball.calm.relax.music.mod.util;

/*
This file is part of AndroidUtils.

AndroidUtils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AndroidUtils. If not, see <https://www.gnu.org/licenses/>.

(ɔ) Yajnesh T
*/

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.os.ConfigurationCompat;
import androidx.core.util.Pair;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig;
import com.talapady.neon.pinball.calm.relax.music.bouncy.R;
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity;

import java.util.Locale;


/**
 * General utils related to activities
 * <p>
 *
 * @author Yajnesh T
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ActivityUtil {

    public static final String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=";

    /**
     * Safe Start activity for result with ActivityOptionsCompacts
     *
     * @param a           current activity
     * @param i           intent to launch
     * @param requestCode requestCode
     * @param options     ActivityOptionsCompat
     */
    public static void startActivityForResult(Activity a,
                                              Intent i,
                                              int requestCode,
                                              ActivityOptionsCompat options) {
        if (a == null) {
            return;
        }
        if (options != null) {
            a.startActivityForResult(
                    i,
                    requestCode,
                    options.toBundle()
            );
        } else {
            a.startActivityForResult(
                    i,
                    requestCode);
        }


    }

    /**
     * Safe Start activity for result with ActivityOptionsCompacts
     *
     * @param f           current fragment
     * @param i           intent to launch
     * @param requestCode requestCode
     * @param options     ActivityOptionsCompat
     */
    public static void startActivityForResult(Fragment f,
                                              Intent i,
                                              int requestCode,
                                              ActivityOptionsCompat options) {
        if (f == null) {
            return;
        }
        if (options != null) {
            f.startActivityForResult(
                    i,
                    requestCode,
                    options.toBundle()
            );
        } else {
            f.startActivityForResult(
                    i,
                    requestCode);
        }
    }

    /**
     * Safe Start activity for result with ActivityOptionsCompacts
     *
     * @param a       current activity
     * @param i       intent to launch
     * @param options ActivityOptionsCompat
     */
    public static void startActivity(Activity a,
                                     Intent i,
                                     ActivityOptionsCompat options) {
        if (a == null) {
            return;
        }
        if (options != null) {
            a.startActivity(i, options.toBundle());
        } else {
            a.startActivity(i);
        }
    }

    /**
     * Get the transition animation between activity for view
     *
     * @param activity current activity
     * @param v        view on which animation is bound
     * @param name     custom name of animation
     * @return ActivityOptionsCompat constructed
     */
    public static ActivityOptionsCompat getTransitionAnimationForProduct(Activity activity, View v, String name) {
        try {
            if (activity == null || v == null) {
                return null;
            }
            ViewCompat.setTransitionName(v, name);

            //noinspection unchecked
            return ActivityOptionsCompat.
                    makeSceneTransitionAnimation(activity,
                            Pair.create(v, ViewCompat.getTransitionName(v))
                    );
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Hide keyboard on launch
     *
     * @param a Activity
     */
    public static void hideKeyBoardOnLaunch(Activity a) {
        try {
            if (a != null && a.getWindow() != null) {
                a.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void openPlayStore(Context c, String packageName) {

        if (c instanceof BaseActivity) {

            Bundle params = new Bundle();
            params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "install");
            params.putString(FirebaseAnalytics.Param.ITEM_ID, packageName);
            params.putString("from", c.getClass().getSimpleName());

            ((BaseActivity) c).logAnalytics(FirebaseAnalytics.Event.SELECT_CONTENT, params);
        }

        if (GenericUtil.isAnyEmpty(c, packageName)) {
            ALog.e("onInstallClicked context or packageName null " + packageName);
            return;
        }

        Uri playStoreUri = Uri.parse(PLAY_STORE_URL + packageName);

        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW).setData(playStoreUri).setPackage("com.android.vending"));
        } catch (Throwable e) {

            if (!openBrowser(c, playStoreUri)) {
                ALog.e(ALog.toast(c, c.getString(R.string.sorry_i_am_uanble_to_open, playStoreUri)), e);
            }
        }
    }

    public static boolean openBrowser(Context c, String url) {
        try {
            return openBrowser(c, Uri.parse(url));
        } catch (Throwable t) {
            ALog.e("Unable to open browser " + url, t);
            return false;
        }
    }

    public static boolean openBrowser(Context c, Uri webpage) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(c.getPackageManager()) != null) {
                c.startActivity(intent);
                return true;
            } else {
                ALog.e("Unable to open browser " + GenericUtil.getStringSafe(webpage));
                return false;
            }
        } catch (Throwable t) {
            ALog.e("Unable to open browser " + GenericUtil.getStringSafe(webpage), t);
            return false;
        }
    }



    public static boolean isFragmentFinishing(Fragment f) {
        return f == null || f.getActivity() == null || f.isRemoving() || f.isDetached() || !f.isAdded();
    }

    public static boolean isActivityFinishing(Activity a) {
        return a == null || a.isFinishing() || a.isDestroyed();
    }



    public static String getMyPackageName(Context c) {
        String pName = BuildConfig.APPLICATION_ID;

        if (GenericUtil.isEmpty(pName) && c != null) {
            pName = c.getApplicationContext().getPackageName();
        }
        if (GenericUtil.isEmpty(pName)) {
            pName = "com.talapady.neon.pinball.calm.relax.music";
        }

        return pName;
    }

    public static void shareMyApp(Context c, String shareUrl) {
        try {
            Intent sendIntent = new Intent().
                    setAction(Intent.ACTION_SEND).
                    putExtra(Intent.EXTRA_TEXT, c.getString(R.string.share_app_text, shareUrl)).
                    setType("text/plain");


            Intent shareIntent =
                    Intent.createChooser(sendIntent, c.getString(R.string.share_app, c.getString(R.string.app_name)));


            if (isSafeToOpenImplicitIntent(c, shareIntent)) {
                c.startActivity(shareIntent);
            } else {
                throw new ActivityNotFoundException("shareMyApp intent not found");
            }
        } catch (Throwable t) {
            ALog.e(ALog.toast(c, R.string.sorry_unable_to_share_app), t);
        }
    }


    public static boolean isSafeToOpenImplicitIntent(Context c, Intent i) {
        try {
            return !GenericUtil.isEmpty(c.getPackageManager().queryIntentActivities(i, 0));
        } catch (Throwable t) {
            return false;
        }
    }

    public static void openSupportEmail(Context c, String email) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO)
                    .setType("text/plain")
                    .setData(Uri.parse("mailto:"))
                    .putExtra(Intent.EXTRA_EMAIL, new String[]{email})
                    .putExtra(Intent.EXTRA_SUBJECT, c.getString(R.string.feedback_help_queries_support_email) )
                    .putExtra(Intent.EXTRA_TEXT, getSupportEmailText(c));

            if (isSafeToOpenImplicitIntent(c, emailIntent)) {
                c.startActivity(emailIntent);
            } else {
                throw new ActivityNotFoundException("openSupportEmail intent not found");
            }
        } catch (Throwable t) {
            ALog.e(ALog.toast(c, R.string.sorry_unable_to_share_app), t);
        }
    }

    public static String getSupportEmailText(Context c) {
        String top = c.getString(R.string.support_email_body);

        String append =
                "\n\n\n\n\n\n--------------------------------------------\n" +
                        "Please keep the below information, as it would help us to understand your message better\n" +
                        "Game version : " + BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE + ")\n" +
                        "Platform : Android\n" +
                        "OS version : " + Build.VERSION.SDK_INT + "\n" +
                        "Device model : " + Build.MODEL + "\n" +
                        "Device Locale : " + getDeviceLocale(c) + "\n" +
                        "Game Language: " + SpManager.getInstance().getLocale() + "\n" +
                        "--------------------------------------------";

        return top + append;
    }

    private static String getDeviceLocale(Context c) {
        try {
            Locale l = ConfigurationCompat.getLocales(c.getResources().getConfiguration()).get(0);
            return l.toString() + " (" + l.getLanguage() + ")";
        } catch (Throwable t) {
            return "";
        }
    }

}
