package com.talapady.neon.pinball.calm.relax.music.mod.menu.help

import android.os.Bundle
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelItem
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import kotlinx.android.synthetic.main.activity_help.*

class HelpActivity : BaseActivity() {
    object CONSTANTS {
        const val helpId = "help_id"

        const val helpKeyMain = "helpKeyMain"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        val helpKey = intent.extras?.getString(CONSTANTS.helpId)

        if (helpKey == null || GenericUtil.isEmpty(helpKey)) {
            return
        }

        tvHelp.text = if (helpKey.startsWith(LevelItem.Constants.LEVEL_ID_PREFIX)) {

            val levelIntId = GenericManager.GenericItem.Util.convertStringIdToIntId(helpKey, LevelItem.Constants.LEVEL_ID_PREFIX)
            val fieldName = "table" + levelIntId + "_rules"
            val tableRulesID = R.string::class.java.getField(fieldName).get(null) as? Int

            (if (tableRulesID != null) (getString(tableRulesID) + "\n\n\n\n") else GenericUtil.EMPTY_STRING) +
                    getString(R.string.generic_help)

        } else {
            getString(R.string.generic_help)
        }
    }
}