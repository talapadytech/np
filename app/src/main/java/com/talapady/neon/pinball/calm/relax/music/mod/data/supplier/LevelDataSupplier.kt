package com.talapady.neon.pinball.calm.relax.music.mod.data.supplier

import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelItem

object LevelDataSupplier : GenericDataSupplier<LevelItem>() {
    override fun getSupply(): ArrayList<LevelItem> {
        return ArrayList<LevelItem>().apply {
            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "1",
                    icon = ImageIconRes(R.drawable.level_0001_hq, R.drawable.level_0001_lq),
                    name = R.string.level_1_name,
                    status = ItemStates(isPreUnlocked = true)
            ))

            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "2",
                    icon = ImageIconRes(R.drawable.level_0002_hq, R.drawable.level_0002_lq),
                    name = R.string.level_2_name,
                    status = ItemStates(isPreUnlocked = true)
            ))

            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "3",
                    icon = ImageIconRes(R.drawable.level_0003_hq, R.drawable.level_0003_lq),
                    name = R.string.level_3_name,
                    status = ItemStates(isPreUnlocked = true),
                    bonus = ArrayList<Bonus?>().apply {
                        add(Bonus(Bonus.BonusType.MULTIPLIER, multiplier = 0.1f))
                    },
            ))

            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "4",
                    icon = ImageIconRes(R.drawable.level_0004_hq, R.drawable.level_0004_lq),
                    name = R.string.level_4_name,
                    status = ItemStates(isPreUnlocked = true),
                    bonus = ArrayList<Bonus?>().apply {
                        add(Bonus(Bonus.BonusType.MULTIPLIER, multiplier = 0.25f))
                    },
            ))

            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "5",
                    icon = ImageIconRes(R.drawable.level_0005_hq, R.drawable.level_0005_lq),
                    name = R.string.level_5_name,
                    bonus = ArrayList<Bonus?>().apply {
                        add(Bonus(Bonus.BonusType.BALL, balls = 1))
                        add(Bonus(Bonus.BonusType.MULTIPLIER, multiplier = 0.5f))
                    },
                    unlockWays = ArrayList<UnlockWay?>().apply {
                        add(UnlockWay(type = UnlockWay.UnlockType.BUY, "0.5"))
                        add(UnlockWay(type = UnlockWay.UnlockType.WATCH_VIDEO_AD, null, false))
                        //add(UnlockWay(type = UnlockWay.UnlockType.TOTAL_SCORE, null, null, 2000000))
                    }))

            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "6",
                    icon = ImageIconRes(R.drawable.level_0006_hq, R.drawable.level_0006_lq),
                        name = R.string.level_6_name,
                    bonus = ArrayList<Bonus?>().apply {
                        add(Bonus(Bonus.BonusType.BALL, balls = 1))
                        add(Bonus(Bonus.BonusType.MULTIPLIER, multiplier = 0.5f))
                    },
                    unlockWays = ArrayList<UnlockWay?>().apply {
                        add(UnlockWay(type = UnlockWay.UnlockType.BUY, "0.5"))
                        add(UnlockWay(type = UnlockWay.UnlockType.WATCH_VIDEO_AD, null, false))
                        //add(UnlockWay(type = UnlockWay.UnlockType.TOTAL_SCORE, null, null, 10000000))
                    }))

            add(LevelItem(
                    id = LevelItem.Constants.LEVEL_ID_PREFIX + "7",
                    icon = ImageIconRes(R.drawable.level_0007_hq, R.drawable.level_0007_lq),
                    name = R.string.level_7_name,
                    bonus = ArrayList<Bonus?>().apply {
                        add(Bonus(Bonus.BonusType.BALL, balls = 2))
                        add(Bonus(Bonus.BonusType.MULTIPLIER, multiplier = 1.00f))
                    },
                    unlockWays = ArrayList<UnlockWay?>().apply {
                        add(UnlockWay(type = UnlockWay.UnlockType.BUY, price = "1.00"))
                    }))
        }
    }
}