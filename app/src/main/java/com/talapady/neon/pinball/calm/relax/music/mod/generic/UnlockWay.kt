package com.talapady.neon.pinball.calm.relax.music.mod.generic

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UnlockWay(var type: UnlockType? = null, var price: String? = null, var skippable: Boolean? = false, var totalScore: Long? = null) : Parcelable {
    @Parcelize
    enum class UnlockType : Parcelable { WATCH_VIDEO_AD, BUY, TOTAL_SCORE }

    fun isValid(): Boolean {
        if (type == null) return false
        if (type == UnlockType.BUY && price == null) return false
        if (type == UnlockType.TOTAL_SCORE && totalScore == null) return false
        return true
    }

    object Util {
        fun removeInvalid(unlockWays: java.util.ArrayList<UnlockWay?>?): Boolean {
            if (unlockWays == null) return false

            var atleastOneValid = false

            val iter = unlockWays.iterator() //checked above

            while (iter.hasNext()) {
                if (iter.next()?.isValid() == true) {
                    atleastOneValid = true
                } else {
                    iter.remove()
                }
            }
            return atleastOneValid
        }

        fun hasAtleastOneValidUnlockWay(unlockWays: java.util.ArrayList<UnlockWay?>?) = removeInvalid(unlockWays)
    }
}