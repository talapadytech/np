package com.talapady.neon.pinball.calm.relax.music.mod.home

import android.os.Bundle
import android.view.View
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.GoogleApiAvailablityCheckActivity
import com.talapady.neon.pinball.calm.relax.music.mod.managers.ScoreManager
import com.talapady.neon.pinball.calm.relax.music.mod.menu.bg.BgActivity
import com.talapady.neon.pinball.calm.relax.music.mod.menu.help.HelpActivity
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelActivity
import com.talapady.neon.pinball.calm.relax.music.mod.menu.settings.SettingsActivity
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.update.UpdateManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.TimeUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.ViewUtil
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : GoogleApiAvailablityCheckActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        mmChangeBg.setOnClickListener {
            logAnalyticsGeneric("bgChangeButton", localClassName)
            launchActivity(BgActivity::class.java)
        }
        mmPlay.setOnClickListener {
            logAnalyticsGeneric("playButton", localClassName)
            launchActivity(LevelActivity::class.java)
        }

        llHelp.setOnClickListener {
            logAnalyticsGeneric("helpButton", localClassName)
            launchActivity(
                HelpActivity::class.java,
                HelpActivity.CONSTANTS.helpId,
                HelpActivity.CONSTANTS.helpKeyMain
            )
        }

        llLang.setOnClickListener {
            logAnalyticsGeneric("langHome", localClassName)
            launchActivity(
                SettingsActivity::class.java,
                SettingsActivity.CONSTANTS.openLang,
                true
            )
        }


        UpdateManager.showUpdateIfNeeded(this)

        ViewUtil.setVisibility(
            if (RemoteConfigManager.langOnHomeScreen()) View.VISIBLE else View.GONE,
            llLang,
            vLayoutGapLang
        )
    }

    override fun onResume() {
        super.onResume()
        tvHighScore.text = getString(R.string.high_score, ScoreManager.getTopHighScore())
        tvHighTime.text = getString(
            R.string.high_time,
            TimeUtil.getHighTimeStringFromSeconds(this, ScoreManager.getTopHighTime())
        )
    }
}