package com.talapady.neon.pinball.calm.relax.music.mod.ads

import android.app.Activity
import com.adcolony.sdk.AdColony
import com.adcolony.sdk.AdColonyInterstitial
import com.adcolony.sdk.AdColonyInterstitialListener
import com.adcolony.sdk.AdColonyZone
import com.facebook.ads.Ad
import com.facebook.ads.InterstitialAd
import com.facebook.ads.InterstitialAdListener
import com.google.android.gms.ads.*
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.gson.Gson
import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.DebugUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.logAnalyticsErrorStatic


object AdsManager {

    enum class AdPlacementIdEnum(val id: String) {
        AD_REWARD_BALL("ad_reward_ball"),
        AD_REWARD_BG("ad_reward_bg"),
        AD_REWARD_LEVEL("ad_reward_level"),
        AD_REWARD_BOUNCY_EXTRA_BALL("ad_reward_bouncy_extra_ball")
    }


    object SUPPORTED_AD_PLATFORMS {
        const val ADMOB = "admob"
        const val FACEBOOK = "facebook"
        const val ADCOLONY = "adcolony"

        val list = listOf(ADMOB, FACEBOOK, ADCOLONY)
    }


    fun loadReward(
        c: Activity?,
        adPlacementEnum: AdPlacementIdEnum?,
        callback: GenericRewardAdView.RewardResult
    ): List<GenericRewardAdView> {
        try {
            if (c == null) {
                logAnalyticsErrorStatic("activity empty")
                return listOf()
            }

            val adType = "reward"

            val adOptions = getAdOptionsFromRemoteConfig(adPlacementEnum, adType)

            if (adOptions == null || GenericUtil.isEmpty(adOptions.items)) {
                //logAnalyticsErrorStatic("$adType adOptions null or empty")
                return listOf()
            }

            val foundItem: MutableList<AdsDataRemoteConfig.AdPlacementId.AdData?> = ArrayList()
            for (item in adOptions.items!!) { //checked above
                if (item == null ||
                    GenericUtil.isAnyEmpty(
                        item.id,
                        item.provider,
                        item.type
                    )
                    ||
                    !GenericUtil.equalsRelaxed(item.type, adType)
                ) continue

                if (SUPPORTED_AD_PLATFORMS.list.contains(item.provider)) {
                    foundItem.add(item)
                }
            }

            if (GenericUtil.isEmpty(foundItem)) {
                logAnalyticsErrorStatic("banner foundItem null, probably no supported ad implentation found")
                return listOf()
            }

            val listOfRewards: MutableList<GenericRewardAdView> = ArrayList(foundItem.size)
            for (item in foundItem) {
                if (item?.id == null) continue

                when (item.provider) {
                    SUPPORTED_AD_PLATFORMS.ADMOB ->
                        rewardAdMob(
                            c,
                            item.id!!,
                            callback
                        )?.let { listOfRewards.add(it) }

                    SUPPORTED_AD_PLATFORMS.FACEBOOK ->
                        rewardFacebook(
                            c,
                            item.id!!,
                            callback
                        )?.let { listOfRewards.add(it) }

                    SUPPORTED_AD_PLATFORMS.ADCOLONY ->
                        rewardAdColony(
                            c,
                            item.id!!,
                            callback
                        )?.let { listOfRewards.add(it) }

                    else -> logAnalyticsErrorStatic("loadReward ${item.provider} not supported yet")
                }
            }
            return listOfRewards
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("loadReward ${ignore.message}")
            return listOf()
        }
    }

    private fun rewardAdColony(
        a: Activity,
        id: String,
        callback: GenericRewardAdView.RewardResult?
    ): GenericRewardAdView? {
        try {

            AdColony.configure(a, a.getString(R.string.adcolonyAppId), id)

            AdColony.setRewardListener { callback?.onShowComplete() }

            var rewardedVideoAd: AdColonyInterstitial? = null

            AdColony.requestInterstitial(id, object : AdColonyInterstitialListener() {
                override fun onRequestFilled(p0: AdColonyInterstitial?) {
                    rewardedVideoAd = p0
                }

                override fun onClosed(ad: AdColonyInterstitial?) {
                    callback?.onClosed()
                }

                override fun onRequestNotFilled(zone: AdColonyZone?) {
                    logAnalyticsErrorStatic("rewardAdColony onRequestNotFilled ${zone?.zoneID}")
                }
            })

            return object : GenericRewardAdView {
                override fun isReady(): Boolean {
                    return try {
                        rewardedVideoAd != null && !rewardedVideoAd!!.isExpired
                    } catch (ignore: Throwable) {
                        false
                    }
                }

                override fun show() {
                    try {
                        rewardedVideoAd?.show()
                    } catch (ignore: Throwable) {
                        logAnalyticsErrorStatic("rewardAdColony show ${ignore.message}")
                    }
                }

                override fun onDestroy() {
                    try {
                        rewardedVideoAd?.destroy()
                    } catch (ignore: Throwable) {
                        logAnalyticsErrorStatic("rewardAdColony onDestroy ${ignore.message}")
                    }
                }
            }

        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("rewardAdColony ${ignore.message}")
            return null
        }
    }

    private fun rewardFacebook(
        a: Activity,
        id: String,
        callback: GenericRewardAdView.RewardResult?
    ): GenericRewardAdView? {
        try {
            val rewardedVideoAd = InterstitialAd(a, id)


            val interstitialAdListener: InterstitialAdListener = object : InterstitialAdListener {
                override fun onInterstitialDisplayed(ad: Ad) {
                    callback?.onShowComplete()
                }

                override fun onInterstitialDismissed(ad: Ad) {
                    callback?.onClosed()
                }


                override fun onError(p0: Ad?, p1: com.facebook.ads.AdError?) {
                    logAnalyticsErrorStatic("rewardFacebook onError() ${p1?.errorMessage}")
                }

                override fun onAdLoaded(ad: Ad) = Unit
                override fun onAdClicked(ad: Ad) = Unit
                override fun onLoggingImpression(ad: Ad) = Unit
            }


            rewardedVideoAd.loadAd(
                rewardedVideoAd.buildLoadAdConfig()
                    .withAdListener(interstitialAdListener)
                    .build()
            )

            return object : GenericRewardAdView {
                override fun isReady(): Boolean {
                    return try {
                        rewardedVideoAd.isAdLoaded && !rewardedVideoAd.isAdInvalidated
                    } catch (ignore: Throwable) {
                        false
                    }
                }

                override fun show() {
                    try {
                        rewardedVideoAd.show()
                    } catch (ignore: Throwable) {
                        logAnalyticsErrorStatic("rewardFacebook show ${ignore.message}")
                    }
                }

                override fun onDestroy() {
                    try {
                        rewardedVideoAd.destroy()
                    } catch (ignore: Throwable) {
                        logAnalyticsErrorStatic("rewardFacebook onDestroy ${ignore.message}")
                    }
                }
            }
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("rewardFacebook ${ignore.message}")
            return null
        }
    }

    private fun rewardAdMob(
        a: Activity,
        adId: String,
        callback: GenericRewardAdView.RewardResult?
    ): GenericRewardAdView? {
        try {

            val finalAdId = if (BuildConfig.DEBUG) DebugUtil.GOOGLE.REWARD_TEST else adId

            var rewardedAd: RewardedAd? = null

            RewardedAd.load(
                a,
                finalAdId,
                AdRequest.Builder().build(),
                object : RewardedAdLoadCallback() {
                    override fun onAdFailedToLoad(adError: LoadAdError) {
                        logAnalyticsErrorStatic("rewardAdMob onAdFailedToLoad() ${adError?.message}")
                        rewardedAd = null
                    }

                    override fun onAdLoaded(ra: RewardedAd) {
                        rewardedAd = ra
                    }
                })


            return object : GenericRewardAdView {
                override fun isReady() = rewardedAd != null

                override fun show() {
                    try {

                        rewardedAd?.fullScreenContentCallback =
                            object : FullScreenContentCallback() {
                                override fun onAdDismissedFullScreenContent() {
                                    callback?.onClosed()
                                }

                                override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                                    logAnalyticsErrorStatic("rewardAdMob onAdFailedToShowFullScreenContent() ${adError?.message}")
                                }

                                override fun onAdShowedFullScreenContent() {
                                    //Log.d(TAG, 'Ad showed fullscreen content.')
                                    // Called when ad is dismissed.
                                    // Don't set the ad reference to null to avoid showing the ad a second time.
                                    rewardedAd = null
                                }
                            }

                        rewardedAd?.show(a
                        ) { callback?.onShowComplete() }

                    } catch (ignore: Throwable) {
                        logAnalyticsErrorStatic("rewardAdMob show ${ignore.message}")
                    }
                }

                override fun onDestroy() {
                    rewardedAd = null
                }
            }
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("rewardAdMob ${ignore.message}")
            return null
        }
    }


    private fun getAdOptionsFromRemoteConfig(
        adPlacementEnum: AdPlacementIdEnum?,
        adType: String
    ): AdsDataRemoteConfig.AdPlacementId? {
        try {
            val adsConfigString = RemoteConfigManager.adsConfig()
            if (GenericUtil.isEmpty(adsConfigString)) {
                //logAnalyticsErrorStatic("$adType adsConfigString remote string null")
                return null
            }

            var adsConfig: AdsDataRemoteConfig? = null
            try {
                adsConfig = Gson().fromJson(adsConfigString, AdsDataRemoteConfig::class.java)
            } catch (t: Throwable) {
                logAnalyticsErrorStatic("$adType adsConfig null $t")
            }

            if (adsConfig == null) {
                logAnalyticsErrorStatic("$adType adsConfig null")
                return null
            }

            if (adPlacementEnum == null) {
                //logAnalyticsErrorStatic("$adType adPlacementEnum null")
                return null
            }

            return GenericUtil.get(adsConfig.data, adPlacementEnum.id)
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("getAdOptionsFromRemoteConfig ${ignore.message}")
            return null
        }
    }
}