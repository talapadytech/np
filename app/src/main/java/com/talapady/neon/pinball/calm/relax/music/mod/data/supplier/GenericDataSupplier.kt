package com.talapady.neon.pinball.calm.relax.music.mod.data.supplier

abstract class GenericDataSupplier<T> {
    abstract fun getSupply(): ArrayList<T>
}