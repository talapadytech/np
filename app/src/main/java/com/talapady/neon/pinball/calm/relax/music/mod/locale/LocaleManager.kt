package com.talapady.neon.pinball.calm.relax.music.mod.locale

import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Patterns
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.entry.SplashActivity
import com.talapady.neon.pinball.calm.relax.music.mod.tulu.TuluSponsorManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.SpManager
import com.yariksoffice.lingver.Lingver
import java.util.*

object LocaleManager {

    lateinit var TULU_LANG_CODE: String

    private var hasInited = false

    fun init(c: Application): LocaleManager {
        if (hasInited) return this
        hasInited = true

        try {
            TULU_LANG_CODE = c.getString(R.string.tulu_lang_code)

            var locale = SpManager.getInstance().locale
            if (GenericUtil.isEmpty(locale)) {
                locale = Locale.getDefault().language

                if (GenericUtil.size(locale) > 2) {
                    locale = locale.substring(0, 2)
                }
            }
            if (GenericUtil.isNotEmpty(locale)) {
                Lingver.init(c, locale)
            }
        } catch (t: Throwable) {
            ALog.e("localeSetup failed", t)
        }
        return this
    }

    fun setLanguage(c: BaseActivity, languageCode: String) {

        //current and new language are same, no op
        if (GenericUtil.equals(
                SpManager.getInstance().locale,
                languageCode
            )
        ) {
            return
        }

        try {
            ALog.toast(c, R.string.please_wait_changing_language)
            Lingver.getInstance().setLocale(c, languageCode)
            SpManager.getInstance().saveLocale(languageCode)

            if (GenericUtil.equals(languageCode, TULU_LANG_CODE)) prefetchSponsorImages(c)

            //TODO: Put a dialog, app will restart in 5 seconds
            Looper.myLooper()?.let {
                Handler(it).postDelayed({
                    try {
                        ActivityCompat.finishAffinity(c)
                        c.launchActivity(SplashActivity::class.java)
                    } catch (t: Throwable) {
                        ALog.e("setLanguage Err: ", t)
                        ALog.toastLong(c, c.getString(R.string.restart_yourself))
                    }
                }, 5000)
            }

        } catch (t: Throwable) {
            ALog.e("setLanguage failed", t)
        }
    }

    private fun prefetchSponsorImages(c: Context) {

        TuluSponsorManager.getListForSettingScreen()?.let { list ->
            for (item in list) {
                try {
                    item?.imageUrl?.let { url ->
                        if (GenericUtil.isNotEmpty(url) &&
                            Patterns.WEB_URL.matcher(url).matches()
                        ) {
                            Glide.with(c)
                                .load(url)
                                .preload()
                        }
                    }
                } catch (ignore: Throwable) {
                }
            }
        }
    }

    fun isTulu() = GenericUtil.equals(SpManager.getInstance().locale, TULU_LANG_CODE)
}

