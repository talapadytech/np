package com.talapady.neon.pinball.calm.relax.music.mod.os

import android.os.Bundle
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.util.ActivityUtil
import kotlinx.android.synthetic.main.activity_open_source_licences.*

class OpenSourceLicencesActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_source_licences)


        tvVP.setOnClickListener {
            logAnalyticOssla("VP")
            ActivityUtil.openBrowser(this, "https://github.com/dozingcat/Vector-Pinball")
        }
        tvOosl.setOnClickListener {
            logAnalyticOssla("OOSL")
            launchActivity(OssLicensesMenuActivity::class.java)
        }
        tvFontAveria.setOnClickListener {
            logAnalyticOssla("Averia")
            ActivityUtil.openBrowser(this, "http://iotic.com/averia/")
        }
        tvFontTuluSri.setOnClickListener {
            logAnalyticOssla("TuluSri")
            ActivityUtil.openBrowser(this, "https://thetulufont.com/")
        }
    }

    private fun logAnalyticOssla(item: String?) {
        logAnalyticsGeneric("OpenSourceLicencesActivity", item)
    }
}