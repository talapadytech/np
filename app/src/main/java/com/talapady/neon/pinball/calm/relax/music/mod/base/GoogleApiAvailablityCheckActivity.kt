package com.talapady.neon.pinball.calm.relax.music.mod.base


import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.SpManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.logAnalyticsErrorStatic


abstract class GoogleApiAvailablityCheckActivity : BaseActivity() {

    val REQUEST_GOOGLE_PLAY_SERVICES = 1972
    val SP_KEY_SHOW_DIALOG = "sp_key_show_dialog"
    val SP_KEY_NUM_TIMES_GOOGLE_DIALOG_SHOWN = "sp_key_num_times_google_dialog_shown"

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        if (RemoteConfigManager.gServicesCheck()) check()
    }

    open fun check() {
        try {
            val api = GoogleApiAvailability.getInstance()
            val code = api.isGooglePlayServicesAvailable(this)

            if (code == ConnectionResult.SUCCESS) return
            else if (api.isUserResolvableError(code)) {
                val timesShown =
                    SpManager.getInstance().getInt(SP_KEY_NUM_TIMES_GOOGLE_DIALOG_SHOWN)
                if (timesShown < 2) {
                    api.showErrorDialogFragment(this, code, REQUEST_GOOGLE_PLAY_SERVICES)
                    // wait for onActivityResult call (see below)
                    SpManager.getInstance()
                        .save(SP_KEY_NUM_TIMES_GOOGLE_DIALOG_SHOWN, timesShown + 1)
                }
            } else {
                showErrDialog(" check: " + api.getErrorString(code))
            }
        } catch (t: Throwable) {
            logAnalyticsErrorStatic("GAPI check, ${t.message}")
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)

            when (requestCode) {
                REQUEST_GOOGLE_PLAY_SERVICES -> {
                    val api = GoogleApiAvailability.getInstance()
                    val code = api.isGooglePlayServicesAvailable(this)
                    if (code != ConnectionResult.SUCCESS) {
                        showErrDialog(" onActivityResult: " + api.getErrorString(code))
                    }
                }
            }
        } catch (t: Throwable) {
            logAnalyticsErrorStatic("GAPI onActivityResult, ${t.message}")
        }
    }

    private fun showErrDialog(errorMessageForAnalytics: String) {
        try {
            if (!SpManager.getInstance().getBool(SP_KEY_SHOW_DIALOG, true)) return

            logAnalyticsErrorStatic("GoogleApiAvailablityCheckActivity:" + errorMessageForAnalytics)

            AlertDialog.Builder(this)
                .setTitle(R.string.google_api_unrecoverable_error_title)
                .setMessage(R.string.google_api_unrecoverable_error_message)
                .setPositiveButton(R.string.ok) { _, _ -> }
                .setNegativeButton(R.string.google_api_unrecoverable_error_negative) { _, _ ->
                    SpManager.getInstance().save(SP_KEY_SHOW_DIALOG, false)
                }
                .show()
        } catch (t: Throwable) {
            logAnalyticsErrorStatic("GAPI showErrDialog, ${t.message}")
        }
    }
}