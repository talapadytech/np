package com.talapady.neon.pinball.calm.relax.music.mod.menu.level

import android.os.Parcelable
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LevelItem(
        private var id: String = GenericUtil.EMPTY_STRING,
        private var icon: ImageIconRes = ImageIconRes(),
        private var status: ItemStates = ItemStates(),
        private var bonus: ArrayList<Bonus?> = ArrayList(),
        private var unlockWays: ArrayList<UnlockWay?> = ArrayList(),
        private var name: Int = GenericUtil.ZERO,
//        var leaderboardId: String = GenericUtil.EMPTY_STRING,
        //var scores: HighScores = HighScores(),
        @Transient private var options: PostStartOptions = PostStartOptions(),
) : GenericManager.GenericItem(), Parcelable {

    object Constants {
        const val LEVEL_ID_PREFIX = "level_"
    }


    @Parcelize
    data class PostStartOptions(var multiplier45secs: Boolean = false, var unlimitedBalls: Boolean = false) : Parcelable

//    @Parcelize
//    data class HighScores(var highScore: Long = GenericUtil.ZERO.toLong(), var maxTimeInSecs: Long = GenericUtil.ZERO.toLong()) : Parcelable


    override fun getId() = id
    override fun getIcon() = icon
    override fun getStatus() = status
    override fun getBonus() = bonus
    override fun getUnlockWays() = unlockWays
    fun getName() = name
    fun getOptions() = options
    override fun getIdPrefix()= Constants.LEVEL_ID_PREFIX
}


