package com.talapady.neon.pinball.calm.relax.music.mod.managers

import com.google.gson.reflect.TypeToken
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseModel
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallItem
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallsManager
import com.talapady.neon.pinball.calm.relax.music.mod.menu.bg.BackgroundItem
import com.talapady.neon.pinball.calm.relax.music.mod.menu.bg.BgManager
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelItem
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.SpManager

abstract class GenericManager<T> where T : GenericManager.GenericItem {


    abstract class GenericItem {

        abstract fun getId(): String
        abstract fun getIcon(): ImageIconRes
        abstract fun getStatus(): ItemStates
        abstract fun getBonus(): ArrayList<Bonus?>
        abstract fun getUnlockWays(): ArrayList<UnlockWay?>
        open fun getIdPrefix() = GenericUtil.EMPTY_STRING

        fun isValid(): Boolean {
            if (GenericUtil.isEmpty(getId()) || !getIcon().isValid()) return false
            if (getStatus().isUnlocked()) return true
            if (GenericUtil.isEmpty(getUnlockWays())) return false

            Bonus.Util.removeInvalid(getBonus())

            return UnlockWay.Util.removeInvalid(getUnlockWays()) //at-least one valid unlock
        }

        fun getExtraBallsCount(): Int {

            //looks fancy :D , should be calculated in single loop than 3 loops, but looks pleasing to the eyes.
            return getBonus()
                    .filter { b -> (b != null && b.type == Bonus.BonusType.BALL) }
                    .map { b -> b?.balls ?: 0 }
                    .sum()
        }

        fun getExtraMultiplierCount(): Float {

            return getBonus()
                    .filter { b -> (b != null && b.type == Bonus.BonusType.MULTIPLIER) }
                    .map { b -> b?.multiplier ?: 0.0f }
                    .sum()
        }

        fun getIntegerId() = Util.convertStringIdToIntId(getId(), getIdPrefix())

        object Util {
            fun convertStringIdToIntId(id: String, prefix: String): Int {
                var onlyNumber = id.replace(prefix, GenericUtil.EMPTY_STRING)

                while (onlyNumber.startsWith('0')) {
                    onlyNumber = onlyNumber.substring(1, onlyNumber.length)
                }

                return Integer.parseInt(onlyNumber)
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            if (other !is GenericItem) return false
            return this.getId() == other.getId()
        }

        override fun hashCode(): Int {
            return this.getId().hashCode()
        }
    }

    class Data<T> {
        var version = 1
        var dataItems = ArrayList<T>()
    }

    private var data = Data<T>()


    protected abstract fun getSaveKey(): String
    protected abstract fun getStaticData(): ArrayList<T>
    protected abstract fun getGenericsHack(): Class<T>
    protected open fun selectionSanitizationRequired(): Boolean = true

    private fun <T> persistList() = SpManager.getInstance().save(getSaveKey(), BaseModel.toJson(data))

    private fun readList(): Data<T>? {
        return BaseModel.fromJson(SpManager.getInstance().getString(getSaveKey()),
                TypeToken.getParameterized(Data::class.java, getGenericsHack()).type)
    }


    fun getData(): ArrayList<T> {
        _getData() //got fresh data in `data`
        sanitizeSelectionState<T>()
        return data.dataItems
    }

    fun getSelectedItem(): T? {
        var found = _getData().find { i -> i.getStatus().isSelected }

        if (found == null) {
            found = GenericUtil.get(_getData(), 0)
        }

        return found
    }

    fun setSelected(item: T) {
        item.getStatus().isSelected = true
        persistList<T>()
    }

    fun setTempUnlocked(id: String) {
        _getData().find { i -> i.getId() == id }?.getStatus()?.isTempUnlocked = true
    }

    private fun _getData(): ArrayList<T> {

        if (!GenericUtil.isEmpty(data.dataItems)) {
            return data.dataItems
        }

        val persistedData = readList()

        if (persistedData != null && !GenericUtil.isEmpty(persistedData.dataItems)) {
            data.dataItems.clear()
            data.dataItems.addAll(persistedData.dataItems)
            return data.dataItems
        }

        val staticData: List<T> = getStaticData()
        data.dataItems.clear()
        data.dataItems.addAll(staticData)
        persistList<T>()

        return data.dataItems
    }


    private fun <T> sanitizeSelectionState() {
        if (!selectionSanitizationRequired()) return

        var foundTempUnlocked = false //user unlocks and selects a ball by watching video, clear that selection on next launch

        data.dataItems.forEach { i ->
            if (!i.getStatus().isUnlocked() && i.getStatus().isSelected) {
                i.getStatus().isSelected = false
                foundTempUnlocked = true
            }
        }

        if (foundTempUnlocked) {
            GenericUtil.get(data.dataItems, 0)?.let {
                it.getStatus().isSelected = true //select first item as selected
                persistList<T>()
            }
        }
    }

    fun markPurchased(skuId: String?): Boolean {
        if (GenericUtil.isEmpty(skuId)) return false

        for (item in _getData()) {
            if (GenericUtil.equals(item.getId(), skuId)) {
                item.getStatus().isPurchased = true
                persistList<T>()
                return true
            }
        }

        return false
    }

    fun markFullGameUnlock() {
        _getData().forEach { item -> item.getStatus().isPurchased = true }
        persistList<T>()
    }


    fun invalidateAllPurchases() {
        _getData().forEach { item -> item.getStatus().isPurchased = false }
        persistList<T>()
    }

    object Util {

        fun markPurchased(skuId: String?) {
            if (skuId == null || GenericUtil.isEmpty(skuId)) {
                ALog.e("mark purchased received null skuId")
                return
            }

            if (GameWideSettings.Constants.FULL_GAME_UNLOCK == skuId) {
                GameWideSettings.setFullGamePurchased(true)
                BallsManager.markFullGameUnlock()
                BgManager.markFullGameUnlock()
                LevelManager.markFullGameUnlock()
                return
            }

            if (skuId.startsWith(BallItem.Constants.BALL_ID_PREFIX)) {
                BallsManager.markPurchased(skuId)
                return
            }

            if (skuId.startsWith(BackgroundItem.Constants.BG_ID_PREFIX)) {
                BgManager.markPurchased(skuId)
                return
            }

            if (skuId.startsWith(LevelItem.Constants.LEVEL_ID_PREFIX)) {
                LevelManager.markPurchased(skuId)
                return
            }

            ALog.e("markPurchased unknown sku received $skuId")
        }

        fun invalidateAllPurchases() {
            GameWideSettings.invalidateAllPurchases()
            BallsManager.invalidateAllPurchases()
            BgManager.invalidateAllPurchases()
            LevelManager.invalidateAllPurchases()
        }
    }

    fun writeToDisk() = persistList<T>()
}
