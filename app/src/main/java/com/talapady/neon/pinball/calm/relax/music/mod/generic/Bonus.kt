package com.talapady.neon.pinball.calm.relax.music.mod.generic

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.text.DecimalFormat

@Parcelize
class Bonus(var type: BonusType? = null, var balls: Int? = null, var multiplier: Float? = null) : Parcelable {


    @Parcelize
    enum class BonusType : Parcelable { BALL, MULTIPLIER }

    fun isValid(): Boolean {
        if (type == null) return false
        if (type == BonusType.BALL && balls == null) return false
        if (type == BonusType.MULTIPLIER && multiplier == null) return false
        return true
    }

    fun getFormattedMultiplier(): String {
        var m = multiplier
        if (m == null) m = 0.0f
        return try {
            Util.formatPattern.format(m)
        } catch (t: Throwable) {
            t.printStackTrace()
            "0.00"
        }
    }

    object Util {
        val formatPattern = DecimalFormat("#.##")
        fun removeInvalid(bonus: java.util.ArrayList<Bonus?>?): Boolean {
            if (bonus == null) return false
            var atleastOneValid = false

            val iter = bonus.iterator()

            while (iter.hasNext()) {
                if (iter.next()?.isValid() == true) {
                    atleastOneValid = true
                } else {
                    iter.remove()
                }
            }
            return atleastOneValid
        }

    }
}