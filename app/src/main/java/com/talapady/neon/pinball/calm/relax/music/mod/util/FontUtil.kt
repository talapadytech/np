package com.talapady.neon.pinball.calm.relax.music.mod.util

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.locale.LocaleManager

object FontUtil {

    private val fontCache: java.util.HashMap<String, Typeface?> = java.util.HashMap<String, Typeface?>()

    fun init(context: Context?) {
        if (context == null) return
        try {
            fontCache[true.toString()] = ResourcesCompat.getFont(context, R.font.tulu_sri_2020)
            fontCache[false.toString()] = ResourcesCompat.getFont(context, R.font.averiasanslibre_bold)
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("FontUtil init ${ignore.message}")
        }

    }

    fun getTypeFace(): Typeface? {
        return fontCache[LocaleManager.isTulu().toString()]
    }

    fun setTypeFace(p: Paint?) {
        try {
            p?.typeface = getTypeFace()
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("FontUtil setTypeFace ${ignore.message}")
        }
    }

    fun setTypeFace(tv: TextView?) {
        try {
            tv?.typeface = getTypeFace()
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("FontUtil setTypeFace ${ignore.message}")
        }
    }
}