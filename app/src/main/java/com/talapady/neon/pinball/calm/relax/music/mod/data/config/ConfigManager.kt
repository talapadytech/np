package com.talapady.neon.pinball.calm.relax.music.mod.data.config

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseModel
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil

object ConfigManager {

    fun getData(callback: GenericCallBack<ConfigModel>? = null, tryFallback: Boolean = true) {
        try {
            val sConfig = RemoteConfigManager.getAppWideConfig()
            if (!GenericUtil.isEmpty(sConfig)) {
                callback?.done(true, BaseModel.fromJson(sConfig, ConfigModel::class.java))
                return
            }

            if (!tryFallback) {
                callback?.done(false, null)
                return
            }
            FirebaseRemoteConfig.getInstance().fetch().addOnCompleteListener { getData(callback, false) }
        } catch (t: Throwable) {
            t.printStackTrace()
            callback?.done(false, null)
            ALog.e("config data exception $t")
        }
    }
}