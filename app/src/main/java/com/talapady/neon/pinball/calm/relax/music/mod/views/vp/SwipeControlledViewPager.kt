package com.talapady.neon.pinball.calm.relax.music.mod.views.vp

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class SwipeControlledViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {
    private var mIsEnabled = true
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (mIsEnabled) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (mIsEnabled) {
            try {
                return super.onInterceptTouchEvent(event)
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
        return false
    }

    fun setPagingEnabled(enabled: Boolean) {
        mIsEnabled = enabled
    }

    fun goNext() {
        adapter?.let {
            setCurrentItem((currentItem + 1) % it.count, true)
        }
    }

    fun goPrevious() {
        adapter?.let {
            setCurrentItem((currentItem - 1 + it.count) % it.count, true)
        }
    }

}