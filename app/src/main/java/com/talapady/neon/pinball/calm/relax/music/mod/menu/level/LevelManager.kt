package com.talapady.neon.pinball.calm.relax.music.mod.menu.level

import com.talapady.neon.pinball.calm.relax.music.mod.data.supplier.LevelDataSupplier
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager

object LevelManager : GenericManager<LevelItem>() {
    override fun getSaveKey() = "LevelManager"
    override fun selectionSanitizationRequired() = false
    override fun getStaticData() = LevelDataSupplier.getSupply()
    override fun getGenericsHack() = LevelItem::class.java
    fun cleanSelectionStates() {
        val currentData = getData()
        for (item in currentData) {
            item.getStatus().isSelected = false
        }
    }
}