package com.talapady.neon.pinball.calm.relax.music.mod.base

import androidx.fragment.app.DialogFragment

abstract class BaseDialogFragment : DialogFragment(), CommonFragmentInterface {
    override fun getActivityReference() = activity
}