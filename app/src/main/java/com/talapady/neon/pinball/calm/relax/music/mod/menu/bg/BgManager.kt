package com.talapady.neon.pinball.calm.relax.music.mod.menu.bg

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.widget.ImageViewCompat
import com.talapady.neon.pinball.calm.relax.music.mod.data.supplier.BgDataSupplier
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.logAnalyticsErrorStatic


object BgManager : GenericManager<BackgroundItem>() {
    override fun getSaveKey() = "BgManager"
    override fun getStaticData() = BgDataSupplier.getSupply()
    override fun getGenericsHack() = BackgroundItem::class.java

    fun setBgImage(iv: AppCompatImageView?) {

        if (iv == null || iv.context == null) return

        val icon = getSelectedItem()?.getIcon() ?: return
        val tint = getSelectedItem()?.getTint() ?: return

        try {
            try {
                iv.setImageResource(icon.iconHq)
            } catch (t: OutOfMemoryError) {
                iv.setImageResource(icon.iconLq)
            }
            ImageViewCompat.setImageTintMode(iv, PorterDuff.Mode.SRC_OVER)
            ImageViewCompat.setImageTintList(iv, ColorStateList.valueOf(Color.argb(tint.a, tint.r, tint.g, tint.b)))

        } catch (ignore: Throwable) {
            ignore.printStackTrace()
            logAnalyticsErrorStatic("setBgImage failed $ignore")
        }
    }

}