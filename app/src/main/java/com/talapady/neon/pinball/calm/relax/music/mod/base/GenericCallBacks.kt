package com.talapady.neon.pinball.calm.relax.music.mod.base

interface GenericCallBack<T> {
    fun done(success: Boolean, data: T? = null)
}

interface GenericCallBack2<T, S> {
    fun done(success: Boolean, data: T?, supportingData: S?)
}

interface GenericStatusCallBack {
    fun done(success: Boolean = true)
}

class NetworkOperations(val startLoading: Boolean)