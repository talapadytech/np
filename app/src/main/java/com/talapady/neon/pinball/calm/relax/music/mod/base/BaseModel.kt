package com.talapady.neon.pinball.calm.relax.music.mod.base

import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import java.lang.reflect.Type

/**
 * Created by Yajnesh on 27-07-17.
 */
open class BaseModel {


    companion object {
        fun <T> fromJson(s: String?, clazz: Class<T>?): T? {
            return try {
                GenericUtil.GSON.fromJson(s, clazz)
            } catch (e: Throwable) {
                e.printStackTrace()
                null
            }
        }


        fun <T> fromJson(s: String?, type: Type?): T? {
            return try {
                GenericUtil.GSON.fromJson(s, type)
            } catch (e: Throwable) {
                e.printStackTrace()
                null
            }
        }

        fun toJson(obj: Any): String {
            return try {
                GenericUtil.GSON.toJson(obj)
            } catch (e: Throwable) {
                e.printStackTrace()
                GenericUtil.EMPTY_STRING
            }
        }
    }
}