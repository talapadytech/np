package com.talapady.neon.pinball.calm.relax.music.mod.views.tv

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.talapady.neon.pinball.calm.relax.music.mod.util.FontUtil

open class CommonTextView : AppCompatTextView {


    constructor(context: Context) : super(context) {
        setCommonProperty()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setCommonProperty()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    ) {
        setCommonProperty()
    }


    private fun setCommonProperty() {
        FontUtil.setTypeFace(this)
    }
}