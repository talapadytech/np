package com.talapady.neon.pinball.calm.relax.music.mod.entry

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.SurfaceHolder
import android.view.View
import com.bumptech.glide.Glide
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.data.config.ConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.home.HomeActivity
import com.talapady.neon.pinball.calm.relax.music.mod.locale.LocaleManager
import com.talapady.neon.pinball.calm.relax.music.mod.tulu.TuluSponsorManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit


class SplashActivity : BaseActivity() {
    private val mediaPlayer = MediaPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        ConfigManager.getData()

        surfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                surfaceCreatedCallback(holder)
            }

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}
            override fun surfaceDestroyed(holder: SurfaceHolder) {}
        })

        getHandler().postDelayed({ startActivity(Intent(this@SplashActivity, HomeActivity::class.java)); this@SplashActivity.finish(); }, TimeUnit.SECONDS.toMillis(3))
    }

    private fun surfaceCreatedCallback(holder: SurfaceHolder) {

        mediaPlayer.setOnPreparedListener { player ->

            getHandler().postDelayed({
                try {

                    val displayMetrics = DisplayMetrics()
                    windowManager.defaultDisplay.getMetrics(displayMetrics)
                    val screenWidth = displayMetrics.widthPixels
                    val screenHeight = displayMetrics.heightPixels

                    surfaceView.pivotX = (screenWidth / 2.0f)
                    surfaceView.pivotY = (screenHeight / 2.0f)
                } catch (ignore: Throwable) {
                    ALog.e("video stretch failed")
                }
                try {
                    player.setVolume(0f, 0f)
                    player.start()
                    setMadeIn()
                } catch (ignore: Throwable) {
                    ignore.printStackTrace()
                }

            }, 10)
        }

        mediaPlayer.setOnCompletionListener { release() }

        try {
            mediaPlayer.setDisplay(holder)
            mediaPlayer.setDataSource(this@SplashActivity, Uri.parse("android.resource://$packageName/${R.raw.splash_video}"))
            mediaPlayer.prepareAsync()
        } catch (ignore: Throwable) {
            ignore.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        release()
    }

    private fun release() {
        try {
            mediaPlayer.stop()
        } catch (ignore: Throwable) {

        }
        try {
            mediaPlayer.reset()
            mediaPlayer.release()
        } catch (ignore: Throwable) {

        }
    }

    private fun setMadeIn() {
        try {
            llTuluSponsor?.visibility = View.GONE

            val tuluSponsor = TuluSponsorManager.getItemForSplash()
            if (LocaleManager.isTulu() && tuluSponsor?.isValid() == true) {
                llTuluSponsor?.visibility = View.VISIBLE
                Glide.with(this).load(tuluSponsor.imageUrl).into(ivTuluSponsor)
                ivTuluText?.text = tuluSponsor.name
            }
        } catch (t: Throwable) {
            ALog.e("setMadeIn failed", t)
        }
    }

}