package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.buy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.viewpager.widget.ViewPager
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseDialogFragment
import com.talapady.neon.pinball.calm.relax.music.mod.base.RewardBaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallItem
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import com.talapady.neon.pinball.calm.relax.music.mod.views.vp.SwipeControlledViewPager
import java.util.*

class BallDialog : BaseDialogFragment() {

    var position: Int = 0
    var data: ArrayList<BallItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.TransparentDialog)
        isCancelable = false


        arguments?.let {
            position = it.getInt(POS)
            data = it.getParcelableArrayList(DATA)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_ball_purchase, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (GenericUtil.isEmpty(data)) {
            dismissAllowingStateLoss()
            logAnalyticsErrorStatic("BallDialog onViewCreated data empty")
            return
        }

        ImageUtil.setBackgroundOomSafe(view.findViewById(R.id.rlRootForBg), R.drawable.popup_bg, R.drawable.popup_bg_lq, javaClass.simpleName)

        view.findViewById<ImageView>(R.id.ivClose)?.setOnClickListener { dismissAllowingStateLoss() }
        val vp = view.findViewById<SwipeControlledViewPager>(R.id.vpBalls)

        val buyContainer = view.findViewById<View>(R.id.buyContainer)
        val freeContainer = view.findViewById<View>(R.id.freeContainer)
        val tvBuyButton = view.findViewById<TextView>(R.id.tvBuyButton)


        view.findViewById<ImageView>(R.id.ivArrowRight)?.let {
            it.visibility = if (GenericUtil.size(data) <= 1) View.GONE else View.VISIBLE
            it.setOnClickListener { vp?.goNext() }
        }
        view.findViewById<ImageView>(R.id.ivArrowLeft)?.setOnClickListener {
            it.visibility = if (GenericUtil.size(data) <= 1) View.GONE else View.VISIBLE
            vp?.goPrevious()
        }


        vp?.offscreenPageLimit = 1
        vp?.adapter = BallsPagerAdapter(childFragmentManager, data!!) //checked above

        vp?.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                performPageSelected(data!!, position, buyContainer, freeContainer, tvBuyButton)
                logAnalyticsGeneric("ballSwiped", GenericUtil.get(data, position)?.getId()
                        ?: GenericUtil.EMPTY_STRING)
            }
        })


        vp?.setCurrentItem(position, false)
        performPageSelected(data!!, position, buyContainer, freeContainer, tvBuyButton)
    }

    private fun performPageSelected(data: ArrayList<BallItem>, pos: Int, buyContainer: View?, freeContainer: View?, tvBuyButton: TextView?) {
        ViewUtil.setVisibility(View.GONE, buyContainer, freeContainer)

        val item = GenericUtil.get(data, pos)
        if (item == null || !item.isValid()) {
            return
        }

        // No valid way to unlock
        if (!UnlockWay.Util.removeInvalid(item.getUnlockWays())) {
            return
        }

        for (i in item.getUnlockWays()) {
            if (i == null || !i.isValid()) {
                continue
            }

            when (i.type) {
                UnlockWay.UnlockType.WATCH_VIDEO_AD -> {

                    if (activity !is RewardBaseActivity) {
                        logAnalyticsErrorStatic("Ball dialog performPageSelected activity is not RewardBaseActivity ${activity?.localClassName}")
                        return
                    }

                    ViewUtil.setVisibility(View.GONE, freeContainer)
                    (activity as? RewardBaseActivity)?.makeViewVisibleWhenAdIsReady(true, freeContainer)
                    freeContainer?.tag = item
                    freeContainer?.setOnClickListener { v ->
                        setFragmentResult(Communicate.REQUEST_KEY,
                                bundleOf(
                                        Communicate.Action.KEY to Communicate.Action.VALUE_HANDLE_WATCH_AD,
                                        Communicate.RESPONSE_DATA_KEY to v.tag as? BallItem?))

                        dismissAllowingStateLoss()
                    }
                }
                UnlockWay.UnlockType.BUY -> {
                    ViewUtil.setVisibility(View.VISIBLE, buyContainer)
                    buyContainer?.tag = item
                    tvBuyButton?.text = i.price
                    buyContainer?.setOnClickListener { v ->
//                        handleBuy(v.tag as? BallItem?)
                        setFragmentResult(Communicate.REQUEST_KEY,
                                bundleOf(
                                        Communicate.Action.KEY to Communicate.Action.VALUE_HANDLE_BUY,
                                        Communicate.RESPONSE_DATA_KEY to v.tag as? BallItem?))
                        dismissAllowingStateLoss()
                    }
                }
                else -> ALog.e("BallsActivity performPageSelected when (i.type) else ${i.type}")
            }
        }
    }


    companion object {

        const val DATA = "data"
        const val POS = "pos"

        object Communicate {
            const val REQUEST_KEY = "Communicate.ball_dialog_to_ball_activity"
            const val RESPONSE_DATA_KEY = "Communicate.response_data_key"

            object Action {
                const val KEY = "Communicate.Action.KEY"
                const val VALUE_HANDLE_WATCH_AD = "Communicate.Action.value_handle_watch_ad"
                const val VALUE_HANDLE_BUY = "Communicate.Action.value_handle_buy"
            }

        }

        @JvmStatic
        fun newInstance(position: Int, data: ArrayList<BallItem>) =
                BallDialog().apply {
                    arguments = Bundle().apply {
                        putInt(POS, position)
                        putParcelableArrayList(DATA, data)
                    }
                }
    }
}

