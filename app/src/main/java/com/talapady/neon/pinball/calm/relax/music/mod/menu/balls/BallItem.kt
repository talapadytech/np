package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls

import android.os.Parcelable
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BallItem(private var id: String = GenericUtil.EMPTY_STRING,
                    private var icon: ImageIconRes = ImageIconRes(),
                    private var status: ItemStates = ItemStates(),
                    private var bonus: ArrayList<Bonus?> = ArrayList(),
                    private var unlockWays: ArrayList<UnlockWay?> = ArrayList())
    : GenericManager.GenericItem(), Parcelable {

    object Constants {
        const val BALL_ID_PREFIX = "ball_"
    }

    override fun getId() = id
    override fun getIcon() = icon
    override fun getStatus() = status
    override fun getBonus() = bonus
    override fun getUnlockWays() = unlockWays
    override fun getIdPrefix() = Constants.BALL_ID_PREFIX
}