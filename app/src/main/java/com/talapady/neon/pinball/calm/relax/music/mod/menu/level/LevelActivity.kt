package com.talapady.neon.pinball.calm.relax.music.mod.menu.level

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.viewpager.widget.ViewPager
import com.android.billingclient.api.SkuDetails
import com.talapady.neon.pinball.calm.relax.music.bouncy.BouncyActivity
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.ads.AdsManager
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.base.RewardBaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.billing.BillingManager
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GameWideSettings
import com.talapady.neon.pinball.calm.relax.music.mod.managers.ScoreManager
import com.talapady.neon.pinball.calm.relax.music.mod.managers.SoundManager
import com.talapady.neon.pinball.calm.relax.music.mod.menu.help.HelpActivity
import com.talapady.neon.pinball.calm.relax.music.mod.menu.offer.BuyAllActivity
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import com.talapady.neon.pinball.calm.relax.music.mod.views.tv.TextViewRegular
import kotlinx.android.synthetic.main.activity_level.*

class LevelActivity : RewardBaseActivity() {

    private val MULTIPLIER_ID_FOR_AD = "multiplier_id_for_ad"
    private val BOUNCY_ACTIVITY_REQUEST_CODE = 10


    override fun getAdPlacementIdEnum() = AdsManager.AdPlacementIdEnum.AD_REWARD_LEVEL

    override fun rewardWatchedSuccess(itemId: String) {
        if (MULTIPLIER_ID_FOR_AD == itemId) {
            LevelManager.getSelectedItem()?.getOptions()?.multiplier45secs = true
            getHandler().postDelayed({ launchActivity(BouncyActivity::class.java, BOUNCY_ACTIVITY_REQUEST_CODE) }, 100)
        } else {
            LevelManager.setTempUnlocked(itemId)
            ALog.toast(this, R.string.congrats_you_ve_unlocked_this_level_for_free)
            performPageSelected(LevelManager.getData(), vpLevels.currentItem)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level)

        cbMultiplier.isChecked = RemoteConfigManager.cbMultiplierAd()

        vpLevels.offscreenPageLimit = 1
        val bgData = LevelManager.getData()
        val bgAdapter = LevelPagerAdapter(supportFragmentManager, bgData)
        vpLevels.adapter = bgAdapter

        ivArrowRight.setOnClickListener { vpLevels.goNext() }
        ivArrowLeft.setOnClickListener { vpLevels.goPrevious() }


        ViewUtil.associateCheckBox(cbMultiplier, cbMultiplierText)


        if (GameWideSettings.isFullGamePurchased()) {
            ViewUtil.associateCheckBox(cbUnlimited, cbUnlimitedText)
        } else {
            launchUnlimitedPurchaseDialogOnClick(cbUnlimited, cbUnlimitedText)
        }

        vpLevels.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                performPageSelected(bgData, position)
                logAnalyticsGeneric("levelSwiped", GenericUtil.get(bgData, position)?.getId()
                        ?: GenericUtil.EMPTY_STRING)
            }
        })

        val startingItemPos = (LevelManager.getSelectedItem()?.getIntegerId() ?: 1) - 1
        vpLevels.setCurrentItem(startingItemPos, false)
        performPageSelected(bgData, startingItemPos)

        //sound
        SoundManager.ViewUtil.refreshSoundViews(ivSound)
        SoundManager.ViewUtil.refreshMusicViews(ivMusic)

        ivSound.setOnClickListener { val action = SoundManager.toggleSound(ivSound); logAnalyticsGeneric("sound$action", localClassName) }
        ivMusic.setOnClickListener { val action = SoundManager.toggleMusic(ivMusic); logAnalyticsGeneric("music$action", localClassName) }

        //sound


        refreshPrices()
    }

    private fun launchUnlimitedPurchaseDialogOnClick(cb: AppCompatCheckBox?, tv: TextViewRegular?) {

        cb?.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                cb.setOnCheckedChangeListener(null)
                cb.isChecked = false
                cb.setOnCheckedChangeListener(this)
                launchActivity(BuyAllActivity::class.java)
                logAnalyticsGeneric("bulkBuyButton", "LevelActivityUlCb")
            }
        })
        tv?.setOnClickListener { launchActivity(BuyAllActivity::class.java); logAnalyticsGeneric("bulkBuyButton", "LevelActivityUlText") }
    }

    private fun performPageSelected(data: List<LevelItem>, position: Int) {
        ViewUtil.setVisibility(View.GONE, unlockedContainer)
        ViewUtil.setVisibility(View.GONE, buyOrAdContainer, freeContainer, buyContainer)
        bSelectLevel.setOnClickListener { }
        freeContainer.setOnClickListener {}
        buyContainer.setOnClickListener {}

        LevelManager.cleanSelectionStates()

        val item = GenericUtil.get(data, position)
        if (item == null || !item.isValid()) {
            return
        }

        ivHelpLevel.setOnClickListener { launchActivity(HelpActivity::class.java, HelpActivity.CONSTANTS.helpId, item.getId()); logAnalyticsGeneric("helpButton", item.getId()) }

        tvTitleLevel.text = if (item.getName() == GenericUtil.ZERO) GenericUtil.EMPTY_STRING else getString(item.getName())

        val scoreItem = ScoreManager.get(item.getId())
        tvHighScore.text = getString(R.string.high_score, scoreItem.highScore)
        tvHighScore.append("\n" + getString(R.string.high_time, TimeUtil.getHighTimeStringFromSeconds(this, scoreItem.highTime)))


        //selectable, unlocked
        if (item.getStatus().isUnlocked()) {
            ViewUtil.setVisibility(View.VISIBLE, unlockedContainer)
            bSelectLevel.setText(R.string.start)

            bSelectLevel.setOnClickListener {

                LevelManager.getSelectedItem()?.getStatus()?.isSelected = false

                LevelManager.setSelected(item)

                item.getOptions().unlimitedBalls = cbUnlimited.isChecked

                if (!cbMultiplier.isChecked) {
                    getHandler().postDelayed({
                        logAnalyticsGeneric("levelSelected", item.getId())
                        launchActivity(BouncyActivity::class.java, BOUNCY_ACTIVITY_REQUEST_CODE)
                    }, 100)
                } else {

                    if (GameWideSettings.isFullGamePurchased()) {
                        rewardWatchedSuccess(MULTIPLIER_ID_FOR_AD)
                    } else {
                        super.showRewardVideo(MULTIPLIER_ID_FOR_AD, false)
                        // flow continues to `rewardWatchedSuccess`
                    }
                }
            }
            return
        }

        // No valid way to unlock
        if (!UnlockWay.Util.removeInvalid(item.getUnlockWays())) {
            return
        }

        ViewUtil.setVisibility(View.VISIBLE, buyOrAdContainer)

        for (i in item.getUnlockWays()) {
            if (i == null || !i.isValid()) {
                continue
            }

            when (i.type) {
                UnlockWay.UnlockType.WATCH_VIDEO_AD -> {
                    ViewUtil.setVisibility(View.GONE, freeContainer)
                    super.makeViewVisibleWhenAdIsReady(true, freeContainer)

                    freeContainer.setOnClickListener {
                        super.showRewardVideo(item.getId(), false)
                        logAnalyticsGeneric("levelAdWatch", item.getId())
                    }

                }
                UnlockWay.UnlockType.BUY -> {
                    setupBuyButton(i.price, item, data, position)
                }
//                UnlockWay.UnlockType.TOTAL_SCORE -> {
//                    i.totalScore?.let {
//
//                        if (it <= ScoreManager.getTopHighScore()) {
//                            item.getStatus().isScoreUnlocked = true
//                            LevelManager.writeToDisk()
//                            performPageSelected(data, position)
//
//                        } else {
//                            ViewUtil.setVisibility(View.VISIBLE, reachScoreContainer)
//                            tvReachScore.text = getString(R.string.reach_score, it)
//                        }
//                    }
//
//                }
                else -> ALog.e("LevelActivity performPageSelected when (i.type) else")
            }
        }
    }


    private fun setupBuyButton(price: String?, item: LevelItem, levelDataList: List<LevelItem>, position: Int) {

        tvBuyButton.text = price
        ViewUtil.setVisibility(View.VISIBLE, buyContainer)

        buyContainer.setOnClickListener {

            logAnalyticsGeneric("levelBuyClick", item.getId())

            BillingManager.PurchaseFlow.startPurchaseFlow(this@LevelActivity, item.getId(), object : GenericCallBack<SkuDetails> {
                override fun done(success: Boolean, data: SkuDetails?) {
                    BillingManager.PurchasedUtil.purchaseVerifyUiLoop(
                            this@LevelActivity,
                            item,
                            success,
                            data,
                            R.string.enjoy_your_new_level,
                            { performPageSelected(levelDataList, position) }
                    )
                }
            })
        }
    }

    private fun refreshPrices() {

        BillingManager.AllItems.getAllAvailableSkus(false, object : GenericCallBack<List<SkuDetails>> {
            override fun done(success: Boolean, data: List<SkuDetails>?) {
                if (ActivityUtil.isActivityFinishing(this@LevelActivity)) return

                val levelData = LevelManager.getData()

                var somethingChanged = false
                for (item in levelData) {

                    val oldPriceItem = item.getUnlockWays().find { uw -> uw?.type == UnlockWay.UnlockType.BUY }
                            ?: continue

                    val found = data?.find { sku -> sku.sku == item.getId() }

                    if (found == null) {
                        ALog.e("Unable to find sku to display bg price. needed= ${item.getId()}")
                        continue
                    }

                    if (!GenericUtil.equals(oldPriceItem.price, found.price)) {
                        somethingChanged = true
                        oldPriceItem.price = found.price
                    }
                }

                if (somethingChanged) {
                    LevelManager.writeToDisk() //not strictly necessary
                    performPageSelected(levelData, vpLevels.currentItem)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == BOUNCY_ACTIVITY_REQUEST_CODE &&
                resultCode == Activity.RESULT_OK &&
                data != null &&
                data.getBooleanExtra(BouncyActivity.GO_HOME_KEY, false)) {
            finish()
        }
    }
}