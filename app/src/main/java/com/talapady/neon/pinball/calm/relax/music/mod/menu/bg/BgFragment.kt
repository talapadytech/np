package com.talapady.neon.pinball.calm.relax.music.mod.menu.bg

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseFragment
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.ViewUtil
import kotlinx.android.synthetic.main.fragment_bg_and_level.*

private const val ARG_PARAM_BG_ITEM = "bgitem"


class BgFragment : BaseFragment() {

    private var item: BackgroundItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            item = it.getParcelable(ARG_PARAM_BG_ITEM)
            if (item == null) ALog.e("null bgitem onCreate")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bg_and_level, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val i = item ?: return

        if (!i.isValid()) {
            ALog.e("Invalid bgitem onViewCreated")
            return
        }

        var bonusText = ""
        if (!GenericUtil.isEmpty(i.getBonus())) {
            for (item in i.getBonus()) {
                if (item == null) continue
                when (item.type) {
                    Bonus.BonusType.BALL -> {
                        if (item.balls != null && item.balls!! > 0) {
                            if (!GenericUtil.isEmpty(bonusText)) bonusText += "\n"
                            bonusText += getString(R.string.bonus_balls, item.balls)
                        }
                    }
                    Bonus.BonusType.MULTIPLIER ->
                        if (item.multiplier != null && item.multiplier!! > 0) {
                            if (!GenericUtil.isEmpty(bonusText)) bonusText += "\n"
                            bonusText += getString(R.string.bonus_mult, item.getFormattedMultiplier())
                        }
                }
            }

        }
        ViewUtil.setVisibility(if (GenericUtil.isEmpty(bonusText)) View.GONE else View.VISIBLE, tvBonus)
        tvBonus.text = bonusText

        val icon = i.getIcon()
        try {
            iv.setImageResource((if (hasOdomOccurred) icon.iconLq else icon.iconHq))
        } catch (oom: OutOfMemoryError) {
            hasOdomOccurred = true
            try {
                iv.setImageResource(icon.iconLq)
            } catch (ignore: Throwable) {
                ignore.printStackTrace()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(item: BackgroundItem) =
                BgFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_PARAM_BG_ITEM, item)
                    }
                }

        @JvmStatic
        var hasOdomOccurred = false
    }
}