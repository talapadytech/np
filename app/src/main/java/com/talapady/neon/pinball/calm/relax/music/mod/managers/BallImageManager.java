package com.talapady.neon.pinball.calm.relax.music.mod.managers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallItem;
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog;
import com.talapady.neon.pinball.calm.relax.music.mod.util.BMUtil;
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil;

public class BallImageManager {


    private static BallImageManager ballImageManagerInstance;
    private Bitmap scaledImage;
    private float radius;
    private int resourceId;
    private Resources resources;

    private BallImageManager() {
    }

    public static BallImageManager getInstance() {
        if (ballImageManagerInstance == null) {
            ballImageManagerInstance = new BallImageManager();
        }
        return ballImageManagerInstance;
    }

    public void setImageResource(BallItem ballItem, Context c) {
        if (GenericUtil.isAnyEmpty(ballItem, c) || ballItem.getIcon().getIconLq() == 0) {
            ALog.e("ball manager setImageResource parameter null");
            return;
        }

        this.resourceId = ballItem.getIcon().getIconLq();
        this.resources = c.getResources();
    }

    public synchronized Bitmap getBall(float radius) {

        if (scaledImage != null && !scaledImage.isRecycled() && radius == this.radius) {
            return scaledImage;
        }

        Bitmap originalImage = BitmapFactory.decodeResource(resources, resourceId);

        BMUtil.recycle(scaledImage);
        scaledImage = BMUtil.scaleCenterCrop(originalImage, (int) (radius * 2), (int) (radius * 2));

        BMUtil.recycle(originalImage);

        this.radius = radius;

        return scaledImage;
    }

    public synchronized void invalidate() {
        BMUtil.recycle(scaledImage);
        scaledImage = null;
    }
}
