package com.talapady.neon.pinball.calm.relax.music.mod.home

import android.os.Bundle
import com.talapady.neon.pinball.calm.relax.music.mod.base.RewardBaseActivity

abstract class BouncyParentActivity : RewardBaseActivity() {

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
    }
}