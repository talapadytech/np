package com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.logAnalyticsErrorStatic
import java.util.*


object RemoteConfigManager {


    object KEYS {
        const val DEBUG_CONFIG_PREFIX = "debug_"

        const val UPDATE_ENABLE_PLAYSTORE = "update_enable_playstore"
        const val UPDATE_ENABLE_LOCAL = "update_enable_local"
        const val PROMPT_TO_VIEW_AD_AT_END_FOR_EXTRA_BALL =
            "prompt_to_view_ad_at_end_for_extra_ball"
        const val SHOW_COMPUSLORY_AD_AFTER_GAVEOVER = "show_compuslory_ad_after_gaveover"
        const val ADS_CONFIG = "ads_config"
        const val APPWIDE_CONFIG = "appwide_config"
        const val LINE_THICKNESS = "line_thickness"

        const val CHECKBOX_45_SEC_MULT = "checkbox_45_sec_mult"
        const val CHECK_G_SERVICES = "check_g_services"
        const val TULU_SPONSOR = "tulu_sponsor"
        const val SHOW_LANG_ON_HOME_SCREEN = "show_lang_on_home_screen"

        val defaultValues = HashMap<String, Any>().apply {
            put(UPDATE_ENABLE_PLAYSTORE, true)
            put(UPDATE_ENABLE_LOCAL, true)
            put(PROMPT_TO_VIEW_AD_AT_END_FOR_EXTRA_BALL, true)
            put(SHOW_COMPUSLORY_AD_AFTER_GAVEOVER, false)
            put(
                ADS_CONFIG,
                "{ \"version\": \"1\", \"data\": { \"ad_reward_ball\": { \"items\": [ { \"provider\": \"admob\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"adcolony\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"facebook\", \"type\": \"reward\", \"id\": \"adid\" } ] }, \"ad_reward_bg\": { \"items\": [ { \"provider\": \"admob\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"adcolony\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"facebook\", \"type\": \"reward\", \"id\": \"adid\" } ] }, \"ad_reward_level\": { \"items\": [ { \"provider\": \"admob\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"adcolony\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"facebook\", \"type\": \"reward\", \"id\": \"adid\" } ] }, \"ad_reward_bouncy_extra_ball\": { \"items\": [ { \"provider\": \"admob\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"adcolony\", \"type\": \"reward\", \"id\": \"adid\" }, { \"provider\": \"facebook\", \"type\": \"reward\", \"id\": \"adid\" } ] } } }"
            )
            put(
                APPWIDE_CONFIG,
                "{ \"version\": \"1\", \"forceUpdate\": [ \"ThisShouldBeVersionNumberNotName\" ], \"feedbackEmail\": \"email\", \"privacyUrl\": \"privacy\", \"termsUrl\": \"terms\", \"socialMainUrl\": \"social\", \"shareUrl\": \"share\" }"
            )
            put(LINE_THICKNESS, 6)
            put(CHECKBOX_45_SEC_MULT, false)
            put(CHECK_G_SERVICES, true)
            put(TULU_SPONSOR, "{}")
            put(SHOW_LANG_ON_HOME_SCREEN, false)
        }
    }

    private var hasInited = false

    fun init(): RemoteConfigManager {
        if (hasInited) return this
        hasInited = true

        try {
            Firebase.remoteConfig.setDefaultsAsync(KEYS.defaultValues).addOnCompleteListener {
                try {
                    Firebase.remoteConfig.fetchAndActivate()
                        .addOnFailureListener { exception -> logAnalyticsErrorStatic("Remote config fetchAndActivate failed $exception") }
                } catch (t: Throwable) {
                    ALog.e("remote config fetchAndActivate failed", t)
                }
            }
        } catch (t: Throwable) {
            ALog.e("remote config setDefaultsAsync failed", t)
        }

        return this
    }

    private fun getFinalKey(key: String): String {
        return if (BuildConfig.DEBUG) {
            KEYS.DEBUG_CONFIG_PREFIX + key
        } else {
            key
        }
    }

    private fun genericString(key: String): String? {
        return try {
            val s = Firebase.remoteConfig[getFinalKey(key)].asString()
            if (GenericUtil.isEmpty(s) || GenericUtil.equalsRelaxed(s, "null")) {
                null
            } else {
                s
            }
        } catch (t: Throwable) {
            ALog.e("$key remote config fetch failed", t)
            null
        }
    }


    private fun genericBool(key: String, defaultValue: Boolean = false): Boolean {

        return try {
            Firebase.remoteConfig[getFinalKey(key)].asBoolean()
        } catch (t: Throwable) {
            ALog.e("$key remote config fetch failed", t)
            defaultValue
        }
    }

    private fun genericInt(key: String): Long {
        return try {
            Firebase.remoteConfig[getFinalKey(key)].asLong()
        } catch (t: Throwable) {
            ALog.e("$key remote config fetch failed", t)
            0
        }
    }

    fun adsConfig() = genericString(KEYS.ADS_CONFIG)
    fun updatePlayStore() = genericBool(KEYS.UPDATE_ENABLE_PLAYSTORE)
    fun updateLocal() = genericBool(KEYS.UPDATE_ENABLE_LOCAL)
    fun promptAdForExtraBall() = genericBool(KEYS.PROMPT_TO_VIEW_AD_AT_END_FOR_EXTRA_BALL)
    fun showAdAfterGameOver() = genericBool(KEYS.SHOW_COMPUSLORY_AD_AFTER_GAVEOVER)
    fun getAppWideConfig() = genericString(KEYS.APPWIDE_CONFIG)
    fun getLineThickness() = genericInt(KEYS.LINE_THICKNESS)
    fun cbMultiplierAd() = genericBool(KEYS.CHECKBOX_45_SEC_MULT)
    fun gServicesCheck() = genericBool(KEYS.CHECK_G_SERVICES, true)
    fun tuluSponsor() = genericString(KEYS.TULU_SPONSOR)
    fun langOnHomeScreen() = genericBool(KEYS.SHOW_LANG_ON_HOME_SCREEN)
}


