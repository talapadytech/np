package com.talapady.neon.pinball.calm.relax.music.mod.update

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.analytics.FirebaseAnalytics
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.data.config.ConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.data.config.ConfigModel
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.ActivityUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.ViewUtil


object UpdateManager {

    object KEYS {
        const val DAYS_FOR_FLEXIBLE_UPDATE = 5
        const val UPDATE_REQUEST_CODE = 41
    }

    private var hasInited = false

    private var appUpdateManager: AppUpdateManager? = null

    fun init(context: Context): UpdateManager {
        if (hasInited) return this
        hasInited = true

        appUpdateManager = AppUpdateManagerFactory.create(context)
        return this
    }

    fun showUpdateIfNeeded(a: Activity) {

        ConfigManager.getData(object : GenericCallBack<ConfigModel> {
            override fun done(success: Boolean, data: ConfigModel?) {
                val appVerCode = GenericUtil.getAppVersion(a)

                if (!success || GenericUtil.isAnyEmpty(data?.forceUpdate, appVerCode)) {
                    googlePlayUpdate(a)
                    return
                }

                //!! checked above
                var localBlock = false
                for (item in data!!.forceUpdate!!) {
                    if (GenericUtil.equalsRelaxed(item, appVerCode)) {
                        localBlock = true
                        break
                    }
                }

                if (localBlock) {
                    localBlockUpdate(a)
                } else {
                    googlePlayUpdate(a)
                }

            }
        })
    }

    private fun localBlockUpdate(a: Activity) {
        if (!RemoteConfigManager.updateLocal()) {
            return
        }
        logAnalyticsGeneric(a, "localUpdateShown")
        try {
            if (a == null) return
            val alertDialog: AlertDialog? = a.let {
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setPositiveButton(
                            R.string.update
                    ) { dialog, _ ->
                        logAnalyticsGeneric(a, "localUpdateInstall")
                        ActivityUtil.openPlayStore(context, ActivityUtil.getMyPackageName(context))
                        ViewUtil.dismissDialog(dialog)
                    }
                    setMessage(R.string.this_version_no_longer_supported)
                    setTitle(R.string.update_app)
                    setIcon(R.mipmap.icon)
                }
                builder.create()
            }

            alertDialog?.show()
            alertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)
                    ?.setTextColor(ContextCompat.getColor(a, R.color.black))
            alertDialog?.getButton(AlertDialog.BUTTON_POSITIVE)
                    ?.setTextColor(ContextCompat.getColor(a, R.color.black))
        } catch (t: Throwable) {
            ALog.e("localBlockUpdate failed", t)
        }
    }

    private fun googlePlayUpdate(a: Activity) {

        if (!RemoteConfigManager.updatePlayStore()) {
            return
        }

        logAnalyticsGeneric(a, "playstoreUpdateProcessStarted")
        appUpdateManager?.appUpdateInfo?.addOnSuccessListener { appUpdateInfo ->
            try {
                if (appUpdateInfo == null) return@addOnSuccessListener

                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                        appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
                ) {

                    val sd = appUpdateInfo.clientVersionStalenessDays()
                    if (sd != null && sd < KEYS.DAYS_FOR_FLEXIBLE_UPDATE
                    ) {
                        return@addOnSuccessListener
                    }
                    if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                        playUpdateCloseAppDialog(a)
                    } else {

                        val listener = object : InstallStateUpdatedListener {
                            override fun onStateUpdate(state: InstallState) {
                                if (state == null) return
                                if (state.installStatus() == InstallStatus.DOWNLOADED) {
                                    playUpdateCloseAppDialog(a)
                                    appUpdateManager?.unregisterListener(this)
                                }

                                if (state.installStatus() == InstallStatus.INSTALLED) {
                                    appUpdateManager?.unregisterListener(this)
                                }
                            }
                        }

                        appUpdateManager?.unregisterListener(listener)
                        appUpdateManager?.registerListener(listener)

                        appUpdateManager?.startUpdateFlowForResult(
                                appUpdateInfo,
                                AppUpdateType.FLEXIBLE,
                                a,
                                KEYS.UPDATE_REQUEST_CODE
                        )
                    }
                }
            } catch (t: Throwable) {
                ALog.e("googlePlayUpdate failed", t)
            }
        }
    }

    private fun playUpdateCloseAppDialog(a: Activity?) {
        try {
            if (a == null) return
            val alertDialog: AlertDialog? = a.let {
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setPositiveButton(
                            R.string.install
                    ) { dialog, _ ->
                        logAnalyticsGeneric(a, "playstoreUpdateInstall")
                        appUpdateManager?.completeUpdate()
                        ViewUtil.dismissDialog(dialog)
                    }
                    setNegativeButton(
                            R.string.cancel
                    ) { dialog, _ ->
                        logAnalyticsGeneric(a, "playstoreUpdateCancel")
                        ViewUtil.dismissDialog(dialog)
                    }
                    setMessage(R.string.download_complete)
                    setTitle(R.string.update_app)
                    setIcon(R.mipmap.icon)
                }
                builder.create()

            }

            logAnalyticsGeneric(a, "playstoreUpdateDialogShown")
            alertDialog?.show()
            alertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)
                    ?.setTextColor(ContextCompat.getColor(a, R.color.black))
            alertDialog?.getButton(AlertDialog.BUTTON_POSITIVE)
                    ?.setTextColor(ContextCompat.getColor(a, R.color.black))
        } catch (t: Throwable) {
            ALog.e("playUpdateCloseAppDialog failed", t)
        }
    }

    private fun logAnalyticsGeneric(a: Activity?, buttonName: String?) {
        val params = Bundle()
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ForceUpdate")
        params.putString(FirebaseAnalytics.Param.ITEM_ID, buttonName)
        (a as? BaseActivity)?.logAnalytics(FirebaseAnalytics.Event.SELECT_CONTENT, params)
    }
}