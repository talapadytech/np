package com.talapady.neon.pinball.calm.relax.music.mod.menu.bg

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.android.billingclient.api.SkuDetails
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.ads.AdsManager
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.base.RewardBaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.billing.BillingManager
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import kotlinx.android.synthetic.main.activity_bg.*

class BgActivity : RewardBaseActivity() {
    override fun getAdPlacementIdEnum() = AdsManager.AdPlacementIdEnum.AD_REWARD_BG

    override fun rewardWatchedSuccess(itemId: String) {
        BgManager.setTempUnlocked(itemId)
        ALog.toast(this, R.string.congrats_you_ve_unlocked_this_background_for_free)
        performPageSelected(BgManager.getData(), vpBackgrounds.currentItem)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bg)

        vpBackgrounds.offscreenPageLimit = 1
        val data = BgManager.getData()
        val bgAdapter = BgPagerAdapter(supportFragmentManager, data)
        vpBackgrounds.adapter = bgAdapter

        ivArrowRight.setOnClickListener { vpBackgrounds.goNext() }
        ivArrowLeft.setOnClickListener { vpBackgrounds.goPrevious() }

        vpBackgrounds.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                performPageSelected(data, position)
                logAnalyticsGeneric("bgSwiped", GenericUtil.get(data, position)?.getId()
                        ?: GenericUtil.EMPTY_STRING)
            }
        })

        val startingItemPos = BgManager.getData().indexOf(BgManager.getSelectedItem())
        vpBackgrounds.setCurrentItem(startingItemPos, false)
        performPageSelected(data, startingItemPos)

        refreshPrices()
    }


    private fun performPageSelected(bgDataList: List<BackgroundItem>, position: Int) {
        ViewUtil.setVisibility(View.GONE, bSelectBg, buyOrAdContainer)
        bSelectBg.setOnClickListener {}
        freeContainer.setOnClickListener {}
        buyContainer.setOnClickListener {}
        val item = GenericUtil.get(bgDataList, position)
        if (item == null || !item.isValid()) {
            return
        }

        //selected
        if (item.getStatus().isSelected) {
            ViewUtil.setVisibility(View.VISIBLE, bSelectBg)
            bSelectBg.setText(R.string.ball_selected)
            return
        }

        //selectable, unlocked
        if (item.getStatus().isUnlocked()) {
            ViewUtil.setVisibility(View.VISIBLE, bSelectBg)
            bSelectBg.setText(R.string.ball_select)
            bSelectBg.setOnClickListener {
                BgManager.getSelectedItem()?.getStatus()?.isSelected = false
                BgManager.setSelected(item)
                performPageSelected(bgDataList, position)

                logAnalyticsGeneric("bgSelected", item.getId())
            }

            return
        }

        // No valid way to unlock
        if (!UnlockWay.Util.removeInvalid(item.getUnlockWays())) {
            return
        }

        ViewUtil.setVisibility(View.VISIBLE, buyOrAdContainer)
        ViewUtil.setVisibility(View.GONE, freeContainer, buyContainer)


        for (i in item.getUnlockWays()) {
            if (i == null || !i.isValid()) {
                continue
            }

            when (i.type) {
                UnlockWay.UnlockType.WATCH_VIDEO_AD -> {

                    ViewUtil.setVisibility(View.GONE, freeContainer)
                    super.makeViewVisibleWhenAdIsReady(true, freeContainer)

                    freeContainer.tag = item.getId()
                    freeContainer.setOnClickListener { v ->
                        val id = v.tag as? String ?: GenericUtil.EMPTY_STRING
                        super.showRewardVideo(id, false)
                        logAnalyticsGeneric("bgAdWatch", id)
                    }
                }
                UnlockWay.UnlockType.BUY -> {
                    setupBuyButton(i.price, item, bgDataList, position)
                }
                else -> ALog.e("Bgactivity performPageSelected when (i.type) else")
            }
        }
    }

    private fun setupBuyButton(price: String?, item: BackgroundItem, bgDataList: List<BackgroundItem>, position: Int) {

        tvBuyButton.text = price
        ViewUtil.setVisibility(View.VISIBLE, buyContainer)

        buyContainer.setOnClickListener {

            logAnalyticsGeneric("bgBuyClick", item.getId())

            BillingManager.PurchaseFlow.startPurchaseFlow(this@BgActivity, item.getId(), object : GenericCallBack<SkuDetails> {
                override fun done(success: Boolean, data: SkuDetails?) {

                    BillingManager.PurchasedUtil.purchaseVerifyUiLoop(
                            this@BgActivity,
                            item,
                            success,
                            data,
                            R.string.enjoy_your_new_bg,
                            { performPageSelected(bgDataList, position) }
                    )
                }
            })
        }
    }


    private fun refreshPrices() {

        BillingManager.AllItems.getAllAvailableSkus(false, object : GenericCallBack<List<SkuDetails>> {
            override fun done(success: Boolean, data: List<SkuDetails>?) {
                if (ActivityUtil.isActivityFinishing(this@BgActivity)) return

                val bgData = BgManager.getData()

                var somethingChanged = false
                for (item in bgData) {

                    val oldPriceItem = item.getUnlockWays().find { uw -> uw?.type == UnlockWay.UnlockType.BUY }
                            ?: continue

                    val found = data?.find { sku -> sku.sku == item.getId() }

                    if (found == null) {
                        ALog.e("Unable to find sku to display bg price. needed= ${item.getId()}")
                        continue
                    }

                    if (!GenericUtil.equals(oldPriceItem.price, found.price)) {
                        somethingChanged = true
                        oldPriceItem.price = found.price
                    }
                }

                if (somethingChanged) {
                    BgManager.writeToDisk() //not strictly necessary
                    performPageSelected(bgData, vpBackgrounds.currentItem)
                }
            }
        })
    }

    override fun bulkBuyButtonVisibilityBehaviour() {
//avoid the superclass call
    }
}