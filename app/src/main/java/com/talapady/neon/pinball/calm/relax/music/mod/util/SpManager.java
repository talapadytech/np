package com.talapady.neon.pinball.calm.relax.music.mod.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig;


/**
 * Shared Preference Manager
 */
public class SpManager {
    private static final String SHARED_PREF_NAME = "ad_data_cache";

    private static SpManager sInstance = null;
    private SharedPreferences mSp;

    private SpManager() {
    }

    /**
     * Singleton instance
     */
    public static synchronized SpManager getInstance() {
        if (sInstance == null) {
            sInstance = new SpManager();
        }
        return sInstance;
    }

    public void init(Context c) {
        if (c == null) {
            ALog.e("Unable to get SharedPref, context is null");
            return;
        }

        try {
            if (BuildConfig.DEBUG) {
                throw new Exception("debug build, saving sp in plaintext");
            }

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                throw new Exception("fallback to normal sp");
            } else {
                MasterKey masterKey = new MasterKey.Builder(c, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
                        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                        .build();

                mSp = EncryptedSharedPreferences.create(c,
                        SHARED_PREF_NAME,
                        masterKey,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                );
            }
        } catch (Throwable e) {
            mSp = c.getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }

    }

    //    public boolean isInited() {
//        return mSp != null;
//    }
//
    @SuppressLint("ApplySharedPref")
    public boolean saveLocale(String locale) {
        if (mSp != null) {
            mSp.edit().putString(Language.LOCALE, locale).commit(); //commit is needed for quick save , do not use apply
            return true;
        }
        return false;
    }

    public String getLocale() {
        if (mSp != null) {
            return mSp.getString(Language.LOCALE, "");
        }
        return null;
    }


    private interface Language {
        String IDENTIFIER_LANG = Language.class.getSimpleName() + "_";
        String LOCALE = IDENTIFIER_LANG + "LOCALE";
    }


    @SuppressLint("ApplySharedPref")
    public boolean save(String key, String value) {
        if (mSp != null) {
            mSp.edit().putString(key, value).commit(); //commit is needed for quick save , do not use apply
            return true;
        }
        return false;
    }

    public String getString(String key) {
        if (mSp != null) {
            return mSp.getString(key, GenericUtil.EMPTY_STRING);
        }
        return GenericUtil.EMPTY_STRING;
    }

    @SuppressLint("ApplySharedPref")
    public boolean save(String key, Boolean value) {
        if (mSp != null) {
            mSp.edit().putBoolean(key, value).commit(); //commit is needed for quick save , do not use apply
            return true;
        }
        return false;
    }

    public Boolean getBool(String key, Boolean defaultValue) {
        if (mSp != null) {
            return mSp.getBoolean(key, defaultValue);
        }
        return defaultValue;
    }

    @SuppressLint("ApplySharedPref")
    public boolean save(String key, int value) {
        if (mSp == null) return false;
        mSp.edit().putInt(key, value).commit(); //commit is needed for quick save , do not use apply
        return true;
    }


    public int getInt(String key) {
        if (mSp == null) return 0;
        return mSp.getInt(key, 0);

    }

}

