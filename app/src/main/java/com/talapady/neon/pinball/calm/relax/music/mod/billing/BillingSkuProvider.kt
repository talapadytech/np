package com.talapady.neon.pinball.calm.relax.music.mod.billing

import com.talapady.neon.pinball.calm.relax.music.mod.managers.GameWideSettings

object BillingSkuProvider {


    private val allSkus = listOf(
           "all your skus go here"
            )


    fun getAllSkus(): List<String> {
        return allSkus
    }
}