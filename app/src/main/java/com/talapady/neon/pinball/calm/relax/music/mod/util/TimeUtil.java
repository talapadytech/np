package com.talapady.neon.pinball.calm.relax.music.mod.util;

import android.content.Context;

import com.talapady.neon.pinball.calm.relax.music.bouncy.R;

public class TimeUtil {

    interface TimeUnits {
        int MINUTE_IN_SECONDS = 60;
        int HOUR_IN_SECONDS = MINUTE_IN_SECONDS * 60;
        int DAY_IN_SECONDS = HOUR_IN_SECONDS * 24;
    }

    public static String getHighTimeStringFromSeconds(Context context, long highTime) {
        if (context == null) return "";

        if (highTime < TimeUnits.MINUTE_IN_SECONDS) {
            return context.getString(R.string.time_unit_second, getTwoDigitValue(highTime));
        }

        if (highTime < TimeUnits.HOUR_IN_SECONDS) {
            long minuteFormatted = highTime / TimeUnits.MINUTE_IN_SECONDS;
            long secondsUnformatted = highTime % TimeUnits.MINUTE_IN_SECONDS;

            return context.getString(R.string.time_unit_minute, getTwoDigitValue(minuteFormatted)) + ":" + getHighTimeStringFromSeconds(context, secondsUnformatted);
        }

        if (highTime < TimeUnits.DAY_IN_SECONDS) {
            long hoursFormatted = highTime / TimeUnits.HOUR_IN_SECONDS;
            long minutesUnformatted = highTime - (hoursFormatted * TimeUnits.HOUR_IN_SECONDS);

            return context.getString(R.string.time_unit_hour, getTwoDigitValue(hoursFormatted)) + ":" + getHighTimeStringFromSeconds(context, minutesUnformatted);
        }

        long daysFormatted = highTime / TimeUnits.DAY_IN_SECONDS;
        long hoursUnformatted = highTime - (daysFormatted * TimeUnits.DAY_IN_SECONDS);

        return context.getString(R.string.time_unit_day, getTwoDigitValue(daysFormatted)) + ":" + getHighTimeStringFromSeconds(context, hoursUnformatted);
    }

    /**
     * Get two digit value from input value
     *
     * @param value value
     * @return <pre>
     *     {@code
     *     1) 2 -> 02
     *     2) 20 -> 20} </pre>
     */
    public static String getTwoDigitValue(long value) {
        StringBuilder result = new StringBuilder();

        if (value < 10) {
            result.append('0');
        }
        result.append(value);

        return result.toString();
    }
}
