package com.talapady.neon.pinball.calm.relax.music.mod.util

import android.os.Bundle
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig


fun logAnalyticsErrorStatic(msg: String) {
    try {
        if (!BuildConfig.DEBUG) {
            val params = Bundle()
            params.putString("errorMessage", msg)
            Firebase.analytics.logEvent("GeneralError", params)
        } else {
            ALog.genuineErrors(msg)
        }
    } catch (ignore: Throwable) {
    }
}
