package com.talapady.neon.pinball.calm.relax.music.mod.ads

data class AdsDataRemoteConfig(
    var version: String? = null,
    var data: Map<String?, AdPlacementId?>? = null
) {
    data class AdPlacementId(
        var items: List<AdData?>? = null
    ) {
        data class AdData(
            var provider: String? = null,
            var type: String? = null,
            var id: String? = null
        )
    }
}