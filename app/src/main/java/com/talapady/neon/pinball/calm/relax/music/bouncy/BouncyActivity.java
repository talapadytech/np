package com.talapady.neon.pinball.calm.relax.music.bouncy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.badlogic.gdx.physics.box2d.Box2D;
import com.talapady.neon.pinball.calm.relax.music.mod.ads.AdsManager;
import com.talapady.neon.pinball.calm.relax.music.mod.home.BouncyParentActivity;
import com.talapady.neon.pinball.calm.relax.music.mod.managers.BallImageManager;
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GameWideSettings;
import com.talapady.neon.pinball.calm.relax.music.mod.managers.ScoreManager;
import com.talapady.neon.pinball.calm.relax.music.mod.managers.SoundManager;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallItem;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallsManager;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.bg.BackgroundItem;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.bg.BgManager;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.help.HelpActivity;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelItem;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.level.LevelManager;
import com.talapady.neon.pinball.calm.relax.music.mod.menu.settings.SettingsActivity;
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager;
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog;
import com.talapady.neon.pinball.calm.relax.music.mod.util.ActivityUtil;
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil;
import com.talapady.neon.pinball.calm.relax.music.mod.util.ImageUtil;
import com.talapady.neon.pinball.calm.relax.music.mod.util.TimeUtil;
import com.talapady.neon.pinball.calm.relax.music.mod.util.ViewUtil;
import com.talapady.neon.pinball.calm.relax.music.mod.views.popup.DecisionPopUp;
import com.talapady.neon.pinball.calm.relax.music.mod.views.popup.PopUpState;
import com.talapady.neon.pinball.calm.relax.music.vectorpinball.model.Field;
import com.talapady.neon.pinball.calm.relax.music.vectorpinball.model.FieldDriver;
import com.talapady.neon.pinball.calm.relax.music.vectorpinball.model.GameState;
import com.talapady.neon.pinball.calm.relax.music.vectorpinball.model.IStringResolver;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BouncyActivity extends BouncyParentActivity {

    public static String GO_HOME_KEY = "go_home";

    static {
        Box2D.init();
    }

    CanvasFieldView canvasFieldView;
    ScoreView scoreView;

    final static int ACTIVITY_PREFERENCES = 1;

    Handler handler = new Handler(Looper.myLooper());

    IStringResolver stringLookupFn = (key, params) -> {
        int stringId = getResources().getIdentifier(key, "string", getPackageName());
        return getString(stringId, params);
    };
    final Field field = new Field(System::currentTimeMillis, stringLookupFn, new VPSoundpool.Player());

    int numberOfLevels;
    int currentLevel = 1;
    List<Long> highScores;
    static int MAX_NUM_HIGH_SCORES = 5;
    static String HIGHSCORES_PREFS_KEY = "highScores";
    static String OLD_HIGHSCORE_PREFS_KEY = "highScore";

    static final float ZOOM_FACTOR = 1.5f;

    // Delay after ending a game, before a touch will start a new game.
    static final long END_GAME_DELAY_MS = 1000;
    Long endGameTime = System.currentTimeMillis() - END_GAME_DELAY_MS;

    FieldDriver fieldDriver = new FieldDriver();
    FieldViewManager fieldViewManager = new FieldViewManager();
    OrientationListener orientationListener;


    private boolean firstTime = false;
    private boolean isResumedFromPauseMenu = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        BallImageManager.getInstance().setImageResource(BallsManager.INSTANCE.getSelectedItem(), this);


        setContentView(R.layout.main);

        BgManager.INSTANCE.setBgImage(findViewById(R.id.mainBgImage));

        this.numberOfLevels = FieldLayoutReader.getNumberOfLevels(this);
        this.currentLevel = getInitialLevel();

        resetFieldForCurrentLevel();


        canvasFieldView = findViewById(R.id.canvasFieldView);
        canvasFieldView.setManager(fieldViewManager);

        canvasFieldView.setZOrderOnTop(true);
        canvasFieldView.getHolder().setFormat(PixelFormat.TRANSPARENT);


        fieldViewManager.setField(field);
        fieldViewManager.setStartGameAction(() -> doStartGame(null));

        scoreView = findViewById(R.id.scoreView);
        scoreView.setField(field);

        fieldDriver.setField(field);
        fieldDriver.setDrawFunction(fieldViewManager::draw);

        highScores = this.highScoresFromPreferencesForCurrentLevel();
        scoreView.setHighScores(highScores);


        findPopUpViews();

        updateFromPreferences();

        // Initialize audio, loading resources in a separate thread.
        VPSoundpool.initSounds(this);
        (new Thread(VPSoundpool::loadSounds)).start();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {//supports haptic feedback
            VPSoundpool.hapticFn = () -> {
                if (GameWideSettings.INSTANCE.isHapticOn()) {
                    try {
                        scoreView.performHapticFeedback(
                                HapticFeedbackConstants.KEYBOARD_TAP,
                                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
                    } catch (Throwable ignore) {
                    }
                }
            };
        }
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        firstTime = true;
    }


    private void paRestartGame() {
        finish();
    }

    private void paHome() {
        setResult(RESULT_OK, new Intent().putExtra(GO_HOME_KEY, true));
        finish();
    }

    private void paHelp() {
        launchActivity(HelpActivity.class,
                HelpActivity.CONSTANTS.helpId,
                LevelManager.INSTANCE.getSelectedItem().getId());
    }

    private void findPopUpViews() {
        canvasFieldView.setOnTouchListener((v, event) -> {

            if (ViewUtil.isAnyVisible(pauseDialog, gameOverDialog, decisionDialog)) {
                return true;
            } else {
                return false;
            }
        });

        scoreView.setOnTouchListener((v, event) -> {

            if (ViewUtil.isAnyVisible(pauseDialog, gameOverDialog, decisionDialog)) {
                return true;
            } else {
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // Attempt to call setSystemUiVisibility(1) which is "low profile" mode.
        try {
            Method setUiMethod = View.class.getMethod("setSystemUiVisibility", int.class);
            setUiMethod.invoke(scoreView, 1);
        } catch (Throwable ignored) {
        }
        // Reset frame rate since app or system settings that affect performance could have changed.
        fieldDriver.resetFrameRate();

        updateFlippersAndZoom();

        if (firstTime) {
            unpauseGame();
            firstTime = false;
        } else if (isResumedFromPauseMenu) {
            unpauseGame();
            isResumedFromPauseMenu = false;
        } else {
            pauseGame();
            showPauseDialog();
        }
    }

    @Override
    public void onPause() {
        pauseGame();
        super.onPause();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // When a game is in progress, pause rather than exit when the back button is pressed.
        // This prevents accidentally quitting the game.
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (field.getGameState().isGameInProgress() && !field.getGameState().isPaused()) {
                pauseGame();
                showPausedButtons();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void pauseGame() {
        VPSoundpool.pauseMusic();
        if (field.getGameState().isPaused()) return;
        field.getGameState().setPaused(true);

        if (orientationListener != null) orientationListener.stop();
        fieldDriver.stop();
    }

    public void unpauseGame() {
        updateFlippersAndZoom();

        if (!field.getGameState().isPaused()) return;

        if (!field.getGameState().isGameStarted()) {
            hideAllPopups();
        }

        field.getGameState().setPaused(false);

        handler.postDelayed(this::tick, 75);
        if (orientationListener != null) orientationListener.start();

        fieldDriver.start();

        if (field.getGameState().isGameInProgress()) {
            hideAllPopups();
        }
    }

    void showPausedButtons() {

        logAnalyticsGeneric("PausePopup", "shown");
        showPauseDialog();
    }

    @Override
    public void onDestroy() {
        VPSoundpool.cleanup();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case ACTIVITY_PREFERENCES:
                updateFromPreferences();
                break;
        }
    }


    // Update settings from preferences, called at launch and when preferences activity finishes.
    void updateFromPreferences() {


        scoreView.setShowFPS(false);

        // If switching line width or OpenGL/Canvas, reset frame rate manager because maximum
        // achievable frame rate may change.
        int lineWidth = (int) RemoteConfigManager.INSTANCE.getLineThickness();

        if (lineWidth != fieldViewManager.getCustomLineWidth()) {
            fieldViewManager.setCustomLineWidth(lineWidth);
        }

        if (canvasFieldView.getVisibility() != View.VISIBLE) {
            canvasFieldView.setVisibility(View.VISIBLE);
            fieldViewManager.setFieldRenderer(canvasFieldView);
        }
        updateFlippersAndZoom();
    }

    private void updateFlippersAndZoom() {
        if (fieldViewManager != null) {
            fieldViewManager.setIndependentFlippers(GameWideSettings.INSTANCE.isIndependentFlippersOn());
            fieldViewManager.setZoom(GameWideSettings.INSTANCE.isZoomOn() ? ZOOM_FACTOR : 1.0f);
        }

        VPSoundpool.setSoundEnabled(SoundManager.INSTANCE.isSoundOn());
        VPSoundpool.setMusicEnabled(SoundManager.INSTANCE.isMusicOn());
    }

    // Called every 100 milliseconds while app is visible, to update score view and high score.
    void tick() {
        scoreView.invalidate();
        scoreView.setFPS(fieldDriver.getAverageFPS());
        updateHighScoreAndButtonPanel();
        handler.postDelayed(this::tick, 100);
    }

    boolean extraBallAdDecisionMade = false;
    boolean extraBallAdDecisionShown = false;
    boolean gotExtraBall = false;
    boolean usedExtraBall = false;

    /**
     * If the score of the current or previous game is greater than the previous high score,
     * update high score in preferences and ScoreView. Also show button panel if game has ended.
     */
    void updateHighScoreAndButtonPanel() {
        // We only need to check once when the game is over, before the button panel is visible.
//        if (buttonPanel.getVisibility() == View.VISIBLE) return;
        if (!field.getGameState().isGameStarted()) return;
        if (field.getGameState().isGameInProgress()) return;
        if (ViewUtil.isDialogShowing(gameOverDialog)) return;


        //do not merge for the sake of understanding
        synchronized (field) {
            GameState state = field.getGameState();
            if (!field.getGameState().isGameInProgress()) {
                // game just ended, show button panel and set end game timestamp

                if (userPressedEndGame) {
                    actuallyEndGame(state);
                    return;
                }

                if (!extraBallAdDecisionMade) {

                    if (!extraBallAdDecisionShown) {
                        extraBallAdDecisionShown = true;
                        //show popup

                        if (!RemoteConfigManager.INSTANCE.promptAdForExtraBall()) {
                            extraBallAdDecisionMade = true;
                            gotExtraBall = false;
                        } else {

                            if (!BouncyActivity.super.isAdReady()) {
                                extraBallAdDecisionMade = true;
                                gotExtraBall = false;
                            } else {
                                hideAllPopups();
                                logAnalyticsGeneric("ExtraBallVideoPopup", "shown");
                                decisionDialog = new DecisionPopUp(currentShownPopup.setAndGet(PopUpState.PopUp.EXTRA_BALL),
                                        this, R.string.get_extra_ball_video, R.string.view_video).show(new DecisionPopUp.ClickActions() {
                                    @Override
                                    public void positive() {
                                        boolean isAdReady = BouncyActivity.super.showRewardVideo(EXTRA_BALL_AFTER_GAME_OVER, true);

                                        if (!isAdReady) {
                                            rewardWatchFailed();
                                        }
                                        //flow continued to rewardWatchedSuccess
                                        logAnalyticsGeneric("ExtraBallVideoPopup", "yes");
                                    }

                                    @Override
                                    public void negative() {
                                        extraBallAdDecisionMade = true;
                                        gotExtraBall = false;
                                        logAnalyticsGeneric("ExtraBallVideoPopup", "no");
                                    }
                                });
                            }
                        }
                    }
                    return;
                }

                if (gotExtraBall && !usedExtraBall) {
                    usedExtraBall = true;
                    field.addExtraBall();
                    field.getGameState().setGameInProgress(true);
                    pauseGame();
                    showPauseDialog();
                } else {
                    actuallyEndGame(state);
                }
            }
        }
    }

    private void actuallyEndGame(GameState state) {
        state.cancelTimer();
        this.endGameTime = System.currentTimeMillis();


        // No high scores for unlimited balls.
        if (!state.hasUnlimitedBalls()) {
            long score = state.getScore();
            // Add to high scores list if the score beats the lowest existing high score,
            // or if all the high score slots aren't taken.
            if (score > highScores.get(this.highScores.size() - 1) ||
                    highScores.size() < MAX_NUM_HIGH_SCORES) {
                this.updateHighScoreForCurrentLevel(score);
            }

            ScoreManager.INSTANCE.setScoreAndTime(
                    LevelManager.INSTANCE.getSelectedItem().getId(),
                    score,
                    state.getTime());

        }

        showGameOverDialog();
        logAnalyticsGeneric("GameOverPopupShown", LevelManager.INSTANCE.getSelectedItem().getId());

        if (RemoteConfigManager.INSTANCE.showAdAfterGameOver()) {
            if (BouncyActivity.super.isAdReady()) {
                getHandler().postDelayed(() -> {
                    if (ActivityUtil.isActivityFinishing(BouncyActivity.this)) return;
                    super.showRewardVideo(RemoteConfigManager.KEYS.SHOW_COMPUSLORY_AD_AFTER_GAVEOVER, true);
                    logAnalyticsGeneric("showAdAfterGameOverShown", LevelManager.INSTANCE.getSelectedItem().getId());
                }, 150);
            }
        }
    }

    private AlertDialog gameOverDialog;

    private void showGameOverDialog() {
        try {
            if (ActivityUtil.isActivityFinishing(this)) return;
            if (gameOverDialog != null && gameOverDialog.isShowing()) return;

            hideAllPopups();

            gameOverDialog = new AlertDialog.Builder(this, R.style.TransparentDialog).setView(getLayoutInflater().inflate(R.layout.dialog_game_over, null)).setCancelable(false).create();
            gameOverDialog.setCanceledOnTouchOutside(false);
            gameOverDialog.show();


            ImageUtil.INSTANCE.setBackgroundOomSafe(gameOverDialog.findViewById(R.id.rlRootForBg), R.drawable.popup_bg, R.drawable.popup_bg_lq, getLocalClassName());

            gameOverDialog.findViewById(R.id.goLlRestart).setOnClickListener(v -> {
                paRestartGame();
                logAnalyticsGeneric("gameOverPopup", "restart");
            });
            gameOverDialog.findViewById(R.id.goLlHelp).setOnClickListener(v -> {
                paHelp();
                logAnalyticsGeneric("gameOverPopup", "help");
            });
            gameOverDialog.findViewById(R.id.goLlHome).setOnClickListener(v -> {
                paHome();
                logAnalyticsGeneric("gameOverPopup", "home");
            });

            gameOverDialog.findViewById(R.id.settingsClickCapture).setOnClickListener(v -> {
                logAnalyticsGeneric("settingsButton", getLocalClassName());
                launchActivity(SettingsActivity.class);
            });


//        FadingRegularTextView fadingScoreView = gameOverDialog.findViewById(R.id.goTvHighScore);
            TextView fadingScoreView = gameOverDialog.findViewById(R.id.goTvHighScore);

            ScoreManager.ScoreData.Score score = ScoreManager.INSTANCE.get(LevelManager.INSTANCE.getSelectedItem().getId());

            String currentScoreAndTime = (getString(R.string.current_score, score.getLastScore())) + ("\n" + getString(R.string.current_time, TimeUtil.getHighTimeStringFromSeconds(this, score.getLastTime())));
            //String highScoreAndTime = (getString(R.string.high_score, score.getHighScore())) + ("\n" + getString(R.string.high_time, TimeUtil.getHighTimeStringFromSeconds(this, score.getHighTime())));

//        fadingScoreView.setTexts(new String[]{currentScoreAndTime, highScoreAndTime});
//        fadingScoreView.setTimeout(5, TimeUnit.SECONDS);
            fadingScoreView.setText(currentScoreAndTime);

//        gameOverDialog.setOnDismissListener(dialog -> {
//            if (fadingScoreView != null) {
//                fadingScoreView.stop();
//            }
//        });


            ImageView goBtnSound = gameOverDialog.findViewById(R.id.goBtnSound);
            ImageView goBtnMusic = gameOverDialog.findViewById(R.id.goBtnMusic);

            goBtnSound.setOnClickListener(v -> {
                SoundManager.INSTANCE.toggleSound(goBtnSound, null);
                logAnalyticsGeneric("sound$action", getLocalClassName());
            });
            goBtnMusic.setOnClickListener(v -> {
                SoundManager.INSTANCE.toggleMusic(goBtnMusic, null);
                logAnalyticsGeneric("music$action", getLocalClassName());
            });


        } catch (Throwable ignore) {
            logAnalyticsGeneric("error", "unable to gameover dialog " + ignore.getMessage());
        }
    }


    final private PopUpState currentShownPopup = new PopUpState();

    private AlertDialog pauseDialog;
    private AlertDialog decisionDialog;

    private void showPauseDialog() {
        try {
            if (ActivityUtil.isActivityFinishing(this)) return;

            hideAllPopups();

            pauseDialog = new AlertDialog.Builder(this, R.style.TransparentDialog).setView(getLayoutInflater().inflate(R.layout.dialog_pause, null)).setCancelable(false).create();
            pauseDialog.setCanceledOnTouchOutside(false);
            pauseDialog.show();

            ImageUtil.INSTANCE.setBackgroundOomSafe(pauseDialog.findViewById(R.id.rlRootForBg), R.drawable.popup_bg, R.drawable.popup_bg_lq, getLocalClassName());

            pauseDialog.findViewById(R.id.gpLlResume).setOnClickListener(v -> {
                isResumedFromPauseMenu = true;
                unpauseGame();
                logAnalyticsGeneric("PausePopup", "resume");
            });

            pauseDialog.findViewById(R.id.gpLlHelp).setOnClickListener(v -> {
                paHelp();
                logAnalyticsGeneric("PausePopup", "help");
            });

            pauseDialog.findViewById(R.id.gpLlRestart).setOnClickListener(v -> {
                hideAllPopups();
                logAnalyticsGeneric("PausePopup", "restart");
                decisionDialog = new DecisionPopUp(currentShownPopup.setAndGet(PopUpState.PopUp.RESTART),
                        this, R.string.are_you_sure, R.string.yes).show(new DecisionPopUp.ClickActions() {
                    @Override
                    public void positive() {
                        paRestartGame();
                        logAnalyticsGeneric("PausePopupRestart", "yes");
                    }

                    @Override
                    public void negative() {
                        showPauseDialog();
                        logAnalyticsGeneric("PausePopupRestart", "no");
                    }
                });
            });


            pauseDialog.findViewById(R.id.gpLlHome).setOnClickListener(v -> {
                hideAllPopups();
                logAnalyticsGeneric("PausePopup", "home");
                decisionDialog = new DecisionPopUp(currentShownPopup.setAndGet(PopUpState.PopUp.HOME),
                        this, R.string.are_you_sure, R.string.yes).show(new DecisionPopUp.ClickActions() {
                    @Override
                    public void positive() {
                        paHome();
                        logAnalyticsGeneric("PausePopupHome", "yes");
                    }

                    @Override
                    public void negative() {
                        showPauseDialog();
                        logAnalyticsGeneric("PausePopupHome", "no");
                    }
                });
            });


            ImageView gpBtnSound = pauseDialog.findViewById(R.id.gpBtnSound);
            ImageView gpBtnMusic = pauseDialog.findViewById(R.id.gpBtnMusic);

            SoundManager.ViewUtil.INSTANCE.refreshSoundViews(gpBtnSound, null);
            SoundManager.ViewUtil.INSTANCE.refreshMusicViews(gpBtnMusic, null);

            gpBtnSound.setOnClickListener(v -> SoundManager.INSTANCE.toggleSound(gpBtnSound, null));
            gpBtnMusic.setOnClickListener(v -> SoundManager.INSTANCE.toggleMusic(gpBtnMusic, null));

            CheckBox independentFlippers = pauseDialog.findViewById(R.id.cbIndependentFlippers);
            CheckBox zoomBall = pauseDialog.findViewById(R.id.cbZoomBall);
            CheckBox cbHaptic = pauseDialog.findViewById(R.id.cbHapticFeedback);

            independentFlippers.setChecked(GameWideSettings.INSTANCE.isIndependentFlippersOn());
            zoomBall.setChecked(GameWideSettings.INSTANCE.isZoomOn());
            cbHaptic.setChecked(GameWideSettings.INSTANCE.isHapticOn());

            ViewUtil.associateCheckBox(independentFlippers, pauseDialog.findViewById(R.id.cbIndependentFlippersText));
            ViewUtil.associateCheckBox(zoomBall, pauseDialog.findViewById(R.id.cbZoomBallText));
            ViewUtil.associateCheckBox(cbHaptic, pauseDialog.findViewById(R.id.cbHapticFeedbackText));

            independentFlippers.setOnCheckedChangeListener((buttonView, isChecked) -> {
                GameWideSettings.INSTANCE.toggleIndependentFlippers(independentFlippers);
                logAnalyticsGeneric("cbIndependentFlippers", isChecked + "-" + getLocalClassName());
            });
            zoomBall.setOnCheckedChangeListener((buttonView, isChecked) -> {
                GameWideSettings.INSTANCE.toggleZoom(zoomBall);
                logAnalyticsGeneric("cbZoomBall", isChecked + "-" + getLocalClassName());
            });

            cbHaptic.setOnCheckedChangeListener((buttonView, isChecked) -> {
                GameWideSettings.INSTANCE.setHaptic(isChecked);
                logAnalyticsGeneric("cbHaptic", isChecked + "-" + getLocalClassName());
            });
        } catch (Throwable ignore) {
            logAnalyticsGeneric("error", "unable to show pause dialog " + ignore.getMessage());
        }
    }

    // Store separate high scores for each field, using unique suffix in prefs key.
    String highScorePrefsKeyForLevel(int theLevel) {
        return HIGHSCORES_PREFS_KEY + "." + theLevel;
    }

    /**
     * Returns a list of the high score stored in SharedPreferences. Always returns a nonempty
     * list, which will be [0] if no high scores have been stored.
     */
    List<Long> highScoresFromPreferences(int theLevel) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String scoresAsString = prefs.getString(highScorePrefsKeyForLevel(theLevel), "");
        if (scoresAsString.length() > 0) {
            try {
                String[] fields = scoresAsString.split(",");
                List<Long> scores = new ArrayList<>();
                for (String f : fields) {
                    scores.add(Long.valueOf(f));
                }
                return scores;
            } catch (NumberFormatException ex) {
                return Collections.singletonList(0L);
            }
        } else {
            // Check pre-1.5 single high score.
            long oldPrefsScore = prefs.getLong(OLD_HIGHSCORE_PREFS_KEY + "." + currentLevel, 0);
            return Collections.singletonList(oldPrefsScore);
        }
    }

    void writeHighScoresToPreferences(int level, List<Long> scores) {
        StringBuilder scoresAsString = new StringBuilder();
        scoresAsString.append(scores.get(0));
        for (int i = 1; i < scores.size(); i++) {
            scoresAsString.append(",").append(scores.get(i));
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(highScorePrefsKeyForLevel(level), scoresAsString.toString());
        editor.commit();
    }

    List<Long> highScoresFromPreferencesForCurrentLevel() {
        return highScoresFromPreferences(currentLevel);
    }

    /**
     * Updates the high score in the ScoreView display, and persists it to SharedPreferences.
     */
    void updateHighScore(int theLevel, long score) {
        List<Long> newHighScores = new ArrayList<>(this.highScores);
        newHighScores.add(score);
        Collections.sort(newHighScores);
        Collections.reverse(newHighScores);
        if (newHighScores.size() > MAX_NUM_HIGH_SCORES) {
            newHighScores = newHighScores.subList(0, MAX_NUM_HIGH_SCORES);
        }
        this.highScores = newHighScores;
        writeHighScoresToPreferences(theLevel, this.highScores);
        scoreView.setHighScores(this.highScores);
    }

    void updateHighScoreForCurrentLevel(long score) {
        updateHighScore(currentLevel, score);
    }

    int getInitialLevel() {
        return LevelManager.INSTANCE.getSelectedItem().getIntegerId();
    }

    // Button action methods defined by android:onClick values in main.xml.
    public void doStartGame(View view) {
        if (field.getGameState().isPaused()) {
            unpauseGame();
            return;
        }
        // Avoids accidental starts due to touches just after game ends.
        if (endGameTime == null || (System.currentTimeMillis() < endGameTime + END_GAME_DELAY_MS)) {
            return;
        }
        if (!field.getGameState().isGameInProgress()) {
            hideAllPopups();

            resetFieldForCurrentLevel();


            int ballsBonus = 0;
            float multBonus = 0;

            BackgroundItem sBg = BgManager.INSTANCE.getSelectedItem();
            if (sBg != null) {
                ballsBonus += sBg.getExtraBallsCount();
                multBonus += sBg.getExtraMultiplierCount();
            }

            BallItem sBall = BallsManager.INSTANCE.getSelectedItem();
            if (sBall != null) {
                ballsBonus += sBall.getExtraBallsCount();
                multBonus += sBall.getExtraMultiplierCount();
            }

            LevelItem sLevel = LevelManager.INSTANCE.getSelectedItem();
            if (sLevel != null) {
                ballsBonus += sLevel.getExtraBallsCount();
                multBonus += sLevel.getExtraMultiplierCount();
            }

            float adMultiplier = 0f;
            int adMultDuration = 0;

            LevelItem li = LevelManager.INSTANCE.getSelectedItem();
            if (li != null && li.getOptions().getMultiplier45secs()) {
                adMultiplier = 1f;
                adMultDuration = 45;
            }

            if (LevelManager.INSTANCE.getSelectedItem().getOptions().getUnlimitedBalls()) {
                field.startGameWithUnlimitedBalls(ballsBonus, multBonus, adMultiplier, adMultDuration);
            } else {
                field.startGame(ballsBonus, multBonus, adMultiplier, adMultDuration);
            }
            VPSoundpool.playStart();
            endGameTime = null;


            field.launchBall();
        }
    }

    boolean userPressedEndGame = false;


    public void scoreViewClicked(View view) {
        if (field.getGameState().isGameInProgress()) {
            if (field.getGameState().isPaused()) {
                unpauseGame();
            } else {
                pauseGame();
                showPausedButtons();
            }
        } else {
            doStartGame(null);
        }
    }


    void resetFieldForCurrentLevel() {
        field.resetForLayoutMap(FieldLayoutReader.layoutMapForLevel(this, currentLevel));
    }

    void hideAllPopups() {
        if (ActivityUtil.isActivityFinishing(this)) return;
        ViewUtil.dismissDialog(pauseDialog, gameOverDialog, decisionDialog);
    }


    @Override
    public void onBackPressed() {
        pauseGame();
        showPausedButtons();
    }

    public static String EXTRA_BALL_AFTER_GAME_OVER = "EXTRA_BALL_AFTER_GAME_OVER";

    @NotNull
    @Override
    public AdsManager.AdPlacementIdEnum getAdPlacementIdEnum() {
        return AdsManager.AdPlacementIdEnum.AD_REWARD_BOUNCY_EXTRA_BALL;
    }

    @Override
    public void rewardWatchedSuccess(@NotNull String itemId) {

        if (GenericUtil.equals(EXTRA_BALL_AFTER_GAME_OVER, itemId)) {
            extraBallAdDecisionMade = true;
            gotExtraBall = true;
            //flow continued to updateHighScoreAndButtonPanel
            ALog.toast(this, R.string.congrats_you_earned_extra_ball);
            logAnalyticsGeneric("ExtraBallVideoPopup", "gotExtraBall");
        }
    }

    @Override
    protected void rewardWatchedSkipped(@NotNull String itemId) {
        if (!extraBallAdDecisionMade && GenericUtil.equals(EXTRA_BALL_AFTER_GAME_OVER, itemId)) {
            extraBallAdDecisionMade = true;
            gotExtraBall = false;
            //flow continued to updateHighScoreAndButtonPanel
            ALog.toast(this, R.string.extra_ball_failure_video_not_watched);
            logAnalyticsGeneric("ExtraBallVideoPopup", "skippedExtraBall");
        }
    }

    private void rewardWatchFailed() {
        if (!extraBallAdDecisionMade) {
            extraBallAdDecisionMade = true;
            gotExtraBall = false;
            //flow continued to updateHighScoreAndButtonPanel
            ALog.toast(this, R.string.extra_ball_failure_video_not_loaded);
            logAnalyticsGeneric("ExtraBallVideoPopup", "failedExtraBall");
        }
    }
}