package com.talapady.neon.pinball.calm.relax.music.mod.tulu

import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseModel
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.SpManager
import java.util.*
import kotlin.collections.ArrayList

object TuluSponsorManager {

    private const val SP_SPLASH_LAST_SHOWN_ITEM = "sp_splash_last_shown_item"

    private var random = Random()

    private val splashScreenItems = ArrayList<SponsorItem>()
    private val settingScreenItems = ArrayList<SponsorItem>()
    private var isSplashShuffle = true
    private var isSettingsShuffle = true


    fun getItemForSplash(): SponsorItem? {
        fillDataIfNeeded()
        if (splashScreenItems.isEmpty()) return null

        val size = GenericUtil.size(splashScreenItems)

        if (isSplashShuffle) {
            return GenericUtil.get(
                splashScreenItems,
                random.nextInt() % size
            )
        }

        val prevIndex = SpManager.getInstance().getInt(SP_SPLASH_LAST_SHOWN_ITEM)
        SpManager.getInstance().save(SP_SPLASH_LAST_SHOWN_ITEM, (prevIndex + 1) % size)
        return GenericUtil.get(splashScreenItems, prevIndex % size)
    }


    fun getListForSettingScreen(): List<SponsorItem?>? {
        fillDataIfNeeded()
        if (settingScreenItems.isEmpty()) return null

        if (isSettingsShuffle) settingScreenItems.shuffle()

        return settingScreenItems;
    }


    private fun fillDataIfNeeded() {

        if (splashScreenItems.isNotEmpty() && settingScreenItems.isNotEmpty()) return

        val sponsorString = RemoteConfigManager.tuluSponsor()
        val data = BaseModel.fromJson(sponsorString, TuluSponsorModel::class.java)

        isSplashShuffle = data?.splash?.shuffle ?: false
        isSettingsShuffle = data?.scroll?.shuffle ?: false

        val map = HashMap<String, SponsorItem>()

        val mainListIter = data?.list?.iterator()
        while (mainListIter?.hasNext() == true) {
            val item = mainListIter.next()
            if (item?.isValid() == true) {
                item.id?.let { map[it] = item }
            }
        }

        val splashIter = data?.splash?.itemIds?.iterator()
        while (splashIter?.hasNext() == true) {
            val item = GenericUtil.get(map, splashIter.next())
            if (item?.isValid() == true) splashScreenItems.add(item)
        }

        val settingsIter = data?.scroll?.itemIds?.iterator()
        while (settingsIter?.hasNext() == true) {
            val item = GenericUtil.get(map, settingsIter.next())
            if (item?.isValid() == true) settingScreenItems.add(item)
        }

    }
}