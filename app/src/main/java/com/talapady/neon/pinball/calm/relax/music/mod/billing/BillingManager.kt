package com.talapady.neon.pinball.calm.relax.music.mod.billing

import android.app.Activity
import android.content.Context
import androidx.annotation.StringRes
import com.android.billingclient.api.*
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericStatusCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.ActivityUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timerTask


object BillingManager {


    private var billingClient: BillingClient? = null

    fun connectionRefresh(c: Context) = ConnUtil.refresh(c)


    object ConnUtil {

        fun refresh(c: Context) {
            if (billingClient == null) {
                billingClient = BillingClient.newBuilder(c)
                        .setListener(PurchaseFlow.purchasesUpdatedListener)
                        .enablePendingPurchases()
                        .build()
            }

            if (!conEstablished && !startConnectionOngoing) {
                startConnection()
            }

            if (GenericUtil.isEmpty(AllItems.allSkus)) {
                AllItems.getAllAvailableSkus(false, null)
            }
        }

        var conEstablished = false
        var startConnectionOngoing = false

        var connectionRetry = 0
        const val MAX_RETRY = 10

        private fun startConnection() {

            startConnectionOngoing = true
            billingClient?.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    startConnectionOngoing = false
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        connectionRetry = 0
                        conEstablished = true


                        PurchasedUtil.queryPurchases(null)
                        AllItems.getAllAvailableSkus(forceRefresh = true, null)
                    }
                }

                override fun onBillingServiceDisconnected() {
                    startConnectionOngoing = false
                    conEstablished = false
                    connectionRetry++
                    if (connectionRetry < MAX_RETRY) {
                        startConnectionOngoing = true
                        Timer().schedule(timerTask { startConnection() }, TimeUnit.SECONDS.toMillis(10))
                    }
                    if (connectionRetry == MAX_RETRY) {
                        connectionRetry = 0
                    }
                }
            })
        }
    }


    object PurchasedUtil {
        fun queryPurchases(callback: GenericStatusCallBack?) {
            try {
                if (!ConnUtil.conEstablished) {
                    callback?.done(false)
                    return
                }

                val purchasesResult = billingClient?.queryPurchases(BillingClient.SkuType.INAPP)?.purchasesList

                if (purchasesResult != null) {

                    GenericManager.Util.invalidateAllPurchases()

                    handlePurchases(purchases = purchasesResult, callBack = object : GenericStatusCallBack {
                        override fun done(success: Boolean) {
                            callback?.done(true)
                        }
                    })

                } else {
                    callback?.done(false)
                }

            } catch (t: Throwable) {
                ALog.e("queryPurchases failed", t)
                callback?.done(false)
            }
        }


        fun handlePurchases(purchases: List<Purchase>, index: Int = 0, callBack: GenericStatusCallBack? = null) {
            if (GenericUtil.isEmpty(purchases) || index >= GenericUtil.size(purchases)) {
                callBack?.done(true)
                return
            }

            val item = GenericUtil.get(purchases, index)

            handlePurchase(item, object : GenericStatusCallBack {
                override fun done(success: Boolean) {
                    handlePurchases(purchases, index + 1, callBack)
                }
            })
        }

        private fun handlePurchase(purchase: Purchase, callBack: GenericStatusCallBack? = null) {
            try {

                if (purchase.purchaseState != Purchase.PurchaseState.PURCHASED) {
                    callBack?.done(false)
                    return
                }
                if (purchase.isAcknowledged) {
                    setPurchased(purchase)
                    callBack?.done(true)
                    return
                }

                val acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.purchaseToken)
                                .build()

                billingClient?.acknowledgePurchase(
                        acknowledgePurchaseParams
                ) { billingResult ->
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        setPurchased(purchase)
                        callBack?.done(true)
                    } else {
                        callBack?.done(false)
                    }
                }

            } catch (t: Throwable) {
                ALog.e("handlePurchase failed", t)
                callBack?.done(false)
            }
        }

        private fun setPurchased(p: Purchase) {
            GenericManager.Util.markPurchased(p.sku)
        }


        fun purchaseVerifyUiLoop(
                a: BaseActivity,
                item: GenericManager.GenericItem,
                success: Boolean,
                sku: SkuDetails?,
                @StringRes successMessage: Int,
                successBlock: () -> Unit
        ) {
            val MAX_RETRY = 10

            a.logPurchase(success,sku?.sku)

            if (ActivityUtil.isActivityFinishing(a)) return

            if (!success) {
                ALog.toast(a, R.string.purchase_failed)
                return
            }

            ALog.toast(a, R.string.processing_purchase)

            val purchaseTimerTask = Timer()
            var currentTry = 1
            purchaseTimerTask.schedule(timerTask {

                if (ActivityUtil.isActivityFinishing(a)) {
                    purchaseTimerTask.cancel()
                    return@timerTask
                }

                if (currentTry++ > MAX_RETRY) {
                    ALog.toast(a, R.string.purchase_processing_failed)
                    purchaseTimerTask.cancel()
                    return@timerTask
                }

                if (item.getStatus().isPurchased) {

                    a.getHandler().postDelayed({
                        ALog.toast(a, successMessage)
                        successBlock()
                    }, TimeUnit.SECONDS.toMillis(1))

                    purchaseTimerTask.cancel()
                }
            }, TimeUnit.SECONDS.toMillis(1), TimeUnit.SECONDS.toMillis(2))
        }
    }


    object AllItems {

        var allSkus: List<SkuDetails>? = null

        fun getAllAvailableSkus(forceRefresh: Boolean = false, callback: GenericCallBack<List<SkuDetails>>?) {

            if (forceRefresh || GenericUtil.isEmpty(allSkus)) {
                queryAllSkuDetails(callback)
                return
            }
            callback?.done(true, allSkus)
        }


        private fun queryAllSkuDetails(callback: GenericCallBack<List<SkuDetails>>?) {
            try {
                if (!ConnUtil.conEstablished) {
                    callback?.done(false)
                    return
                }


                val params = SkuDetailsParams.newBuilder()
                params.setSkusList(BillingSkuProvider.getAllSkus()).setType(BillingClient.SkuType.INAPP)
                billingClient?.querySkuDetailsAsync(
                        params.build()
                ) { billingResult, skuDetailsList ->

                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                        allSkus = skuDetailsList
                        callback?.done(true, skuDetailsList)
                    } else {
                        callback?.done(false)
                    }
                }

            } catch (t: Throwable) {
                ALog.e("querySkuDetails failed", t)
                callback?.done(false)
            }
        }
    }

    object PurchaseFlow {

        private var purchaseItemNameForCallback: SkuDetails? = null
        private var purchaseCallback: GenericCallBack<SkuDetails>? = null

        val purchasesUpdatedListener =
                PurchasesUpdatedListener { billingResult, purchases ->
                    try {

                        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {

                            PurchasedUtil.handlePurchases(purchases = purchases, callBack = object : GenericStatusCallBack {
                                override fun done(success: Boolean) {
                                    purchaseCallback?.done(true, purchaseItemNameForCallback)
                                }
                            })
                            return@PurchasesUpdatedListener
                        }

                        if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                            purchaseCallback?.done(false)
                            return@PurchasesUpdatedListener
                        }

                        purchaseCallback?.done(false)
                        ALog.e("purchasesUpdatedListener failed ${billingResult.responseCode} ${billingResult.debugMessage}")

                    } catch (t: Throwable) {
                        purchaseCallback?.done(false)
                        ALog.e("purchasesUpdatedListener failed", t)
                    }
                }

        fun startPurchaseFlow(
                a: Activity?,
                skuDetails: SkuDetails?,
                callBack: GenericCallBack<SkuDetails>
        ) {

            if (skuDetails == null || a == null) {
                ALog.e("startPurchaseFlow failed, skuDetails null")
                callBack.done(false)
                return
            }

            (a as? BaseActivity)?.logAnalyticsGeneric("purchaseStarted", skuDetails.sku)

            try {
                val flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails)
                        .build()
                val responseCode = billingClient?.launchBillingFlow(a, flowParams)?.responseCode

                if (responseCode != BillingClient.BillingResponseCode.OK) {
                    ALog.e("startPurchaseFlow failed, $responseCode")
                    callBack.done(false)
                    return
                }
                purchaseCallback = callBack
                purchaseItemNameForCallback = skuDetails
            } catch (t: Throwable) {
                ALog.e("startPurchaseFlow failed", t)
                callBack.done(false)
            }
        }

        fun startPurchaseFlow(
                a: Activity?,
                skuString: String?,
                callBack: GenericCallBack<SkuDetails>
        ) {
            startPurchaseFlow(a, AllItems.allSkus?.find { s -> GenericUtil.equals(s.sku, skuString) }, callBack)
        }
    }
}