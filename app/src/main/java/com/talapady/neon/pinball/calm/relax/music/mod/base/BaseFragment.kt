package com.talapady.neon.pinball.calm.relax.music.mod.base

import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment(), CommonFragmentInterface {
    override fun getActivityReference() = activity
}