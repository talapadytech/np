package com.talapady.neon.pinball.calm.relax.music.mod.generic

import android.os.Parcelable
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageIconRes(
        var iconHq: Int = GenericUtil.ZERO,
        var iconLq: Int = GenericUtil.ZERO,
) : Parcelable {
    fun isValid() = !(iconHq == GenericUtil.ZERO || iconLq == GenericUtil.ZERO)
}
