package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.ViewUtil
import kotlinx.android.synthetic.main.item_ball.view.*


class BallsAdapter(private val dataSet: ArrayList<BallItem>, private val listener: ItemClickListener) : RecyclerView.Adapter<BallsAdapter.ViewHolder>() {

    interface ItemClickListener {
        fun onUnlockClick(position: Int?, data: ArrayList<BallItem>)
        fun onSelectClick(ballItem: BallItem?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_ball, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    var lastAnimatedBallId = GenericUtil.EMPTY_STRING

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val i = GenericUtil.get(dataSet, position)

        if (i == null || !i.isValid()) {
            ALog.e("onBindViewHolder invalid item ${i?.toString()}")

            holder.itemView.ivThumb.setImageResource(0)
            holder.itemView.rlTopRoot.setBackgroundResource(0)
            ViewUtil.setVisibility(View.GONE, holder.itemView.tvBottom)
            return
        }

        holder.itemView.ivThumb.setImageResource(i.getIcon().iconHq)
        holder.itemView.tvMultiplier.text = GenericUtil.EMPTY_STRING
        holder.itemView.tvBottom.setOnClickListener {}

        //1. balls can have only bonus type: MULTIPLIER
        //2. balls can have only ONE bonus multiplier
        GenericUtil.get(i.getBonus().filter { bonus -> bonus != null && bonus.isValid() && bonus.type == Bonus.BonusType.MULTIPLIER }, 0)?.getFormattedMultiplier()?.let {
            with(holder.itemView.tvMultiplier) {
                text = context.getString(R.string.bonus_mult_simple, it)
                setOnClickListener { _ -> ALog.toast(context, context.getString(R.string.bonus_mult, it)) }
            }
        }

        //Selected
        if (i.getStatus().isSelected) {
            holder.itemView.rlTopRoot.setBackgroundResource(R.drawable.balls_top_pik_lq)
            holder.itemView.llBottomContainer.setBackgroundResource(R.drawable.balls_bottom_single_pink_lq)

            holder.itemView.tvBottom?.let {
                ViewUtil.setVisibility(View.VISIBLE, it)
                it.setText(R.string.ball_selected)
            }

            if (lastAnimatedBallId != i.getId()) {//don't animate when user is scrolling
                try {
                    holder.itemView.ivThumb?.animate()?.rotationBy(360F)?.setDuration(400)?.setInterpolator(LinearInterpolator())?.start()
                } catch (ignore: Throwable) {
                }
                lastAnimatedBallId = i.getId()
            }
            return
        }

        //Unlocked, can select
        if (i.getStatus().isUnlocked()) {

            holder.itemView.rlTopRoot.setBackgroundResource(R.drawable.balls_top_blue_lq)
            holder.itemView.llBottomContainer.setBackgroundResource(R.drawable.balls_bottom_single_blue_lq)

            holder.itemView.tvBottom?.let {
                ViewUtil.setVisibility(View.VISIBLE, it)
                it.setText(R.string.ball_select)
                it.tag = i
                it.setOnClickListener { v ->
                    listener?.onSelectClick(v.tag as? BallItem?)
                }
            }
            return
        }


        holder.itemView.rlTopRoot.setBackgroundResource(R.drawable.balls_top_blue_lq)
        holder.itemView.llBottomContainer.setBackgroundResource(R.drawable.balls_bottom_single_blue_lq)

        holder.itemView.tvBottom?.let {
            ViewUtil.setVisibility(View.VISIBLE, it)
            it.setText(R.string.button_unlock)
            it.tag = position
            it.setOnClickListener { v ->
                listener?.onUnlockClick(v.tag as? Int?, dataSet)
            }
        }
    }

    override fun getItemCount(): Int {
        return GenericUtil.size(dataSet)
    }
}