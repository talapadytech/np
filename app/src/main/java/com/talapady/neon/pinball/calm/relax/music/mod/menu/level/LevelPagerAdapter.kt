package com.talapady.neon.pinball.calm.relax.music.mod.menu.level

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseFragment
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil

class LevelPagerAdapter(fm: FragmentManager, private var data: List<LevelItem>) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): BaseFragment =
            LevelFragment.newInstance(GenericUtil.get(data, position))

    override fun getCount() = GenericUtil.size(data)
}