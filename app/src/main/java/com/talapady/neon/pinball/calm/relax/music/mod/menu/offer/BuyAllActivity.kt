package com.talapady.neon.pinball.calm.relax.music.mod.menu.offer

import android.os.Bundle
import com.android.billingclient.api.SkuDetails
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.billing.BillingManager
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GameWideSettings
import com.talapady.neon.pinball.calm.relax.music.mod.util.ALog
import com.talapady.neon.pinball.calm.relax.music.mod.util.ActivityUtil
import kotlinx.android.synthetic.main.activity_balls.*
import kotlinx.android.synthetic.main.activity_buy_all.*

class BuyAllActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_all)

        bottomMainAction.setOnClickListener {

            logAnalyticsGeneric(GameWideSettings.Constants.FULL_GAME_UNLOCK, localClassName)

            BillingManager.PurchaseFlow.startPurchaseFlow(this, GameWideSettings.Constants.FULL_GAME_UNLOCK, object : GenericCallBack<SkuDetails> {
                override fun done(success: Boolean, data: SkuDetails?) {
                    logPurchase(success,data?.sku)
                    if (success) {
                        ALog.toast(this@BuyAllActivity, R.string.enjoy_full_game)
                        getHandler().postDelayed({
                            if (ActivityUtil.isActivityFinishing(this@BuyAllActivity))
                                return@postDelayed
                            finish()
                        }, 1000)

                    } else {
                        ALog.toast(this@BuyAllActivity, R.string.purchase_failed)
                    }
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()

        BillingManager.AllItems.getAllAvailableSkus(false, object : GenericCallBack<List<SkuDetails>> {
            override fun done(success: Boolean, data: List<SkuDetails>?) {

                if (ActivityUtil.isActivityFinishing(this@BuyAllActivity)) return

                val found = data?.find { sku -> sku.sku == GameWideSettings.Constants.FULL_GAME_UNLOCK }

                if (found != null) {
                    bottomMainAction.text = found.price
                } else {
                    ALog.e("error getting FULL_GAME_UNLOCK price")
                }

            }
        })

    }
}