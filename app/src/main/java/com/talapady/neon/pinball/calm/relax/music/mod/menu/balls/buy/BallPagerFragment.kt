package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.buy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseFragment
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.util.ImageUtil

class BallPagerFragment : BaseFragment() {

    private var imageIcon: ImageIconRes? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            imageIcon = it.getParcelable(IMAGE_RES)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_ball_pager, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ImageUtil.setImageOomSafe(view.findViewById(R.id.ivBuyBall), imageIcon?.iconHq, imageIcon?.iconLq, javaClass.simpleName)
    }

    companion object {

        const val IMAGE_RES = "IMAGE_RES"

        @JvmStatic
        fun newInstance(imageResId: ImageIconRes) =
                BallPagerFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(IMAGE_RES, imageResId)
                    }
                }
    }
}