package com.talapady.neon.pinball.calm.relax.music.mod.menu.bg

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseFragment
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil


/**
 * Created by Yajnesh on 26-04-17.
 */
class BgPagerAdapter(fm: FragmentManager, private var data: List<BackgroundItem>) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): BaseFragment =
            BgFragment.newInstance(GenericUtil.get(data, position))

    override fun getCount() = GenericUtil.size(data)
}