package com.talapady.neon.pinball.calm.relax.music.mod.managers

import android.widget.CheckBox

object GameWideSettings : GenericSettingsManger<GameWideSettings.GameWideData>() {
    override fun getSaveKey() = "GameWideSettings" //don't use getClassName, might get stripped during proguard

    object Constants {
        const val FULL_GAME_UNLOCK = "full_game_unlock"
    }

    class GameWideData(var independentFlippers: Boolean = true, var zoom: Boolean = false, var fullGamePurchased: Boolean = false, var useHaptic: Boolean = true) : GenericSettingsManger.GenericSettingItem()

    init {
        data = read(GameWideData::class.java) ?: GameWideData()
    }

    private fun setIndependentFlippersState(isOn: Boolean) {
        data.independentFlippers = isOn
        persist()
    }

    private fun setZoomState(isOn: Boolean) {
        data.zoom = isOn
        persist()
    }

    public fun setHaptic(isOn: Boolean) {
        data.useHaptic = isOn
        persist()
    }

    fun toggleIndependentFlippers(cb: CheckBox? = null) {
        with(!isIndependentFlippersOn()) {
            setIndependentFlippersState(this)
            cb?.isChecked = this
        }
    }

    fun toggleZoom(cb: CheckBox? = null) {
        with(!isZoomOn()) {
            setZoomState(this)
            cb?.isChecked = this
        }
    }

    fun isIndependentFlippersOn(): Boolean = data.independentFlippers
    fun isZoomOn(): Boolean = data.zoom
    fun isHapticOn(): Boolean = data.useHaptic

    fun isFullGamePurchased() = data.fullGamePurchased

    fun setFullGamePurchased(p: Boolean) {
        data.fullGamePurchased = p
        persist()
    }

    fun invalidateAllPurchases() = setFullGamePurchased(false)
}