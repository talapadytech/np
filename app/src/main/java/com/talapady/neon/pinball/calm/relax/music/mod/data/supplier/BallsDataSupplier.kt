package com.talapady.neon.pinball.calm.relax.music.mod.data.supplier

import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallItem

object BallsDataSupplier : GenericDataSupplier<BallItem>() {

    override fun getSupply(): ArrayList<BallItem> {
        return ArrayList<BallItem>().apply {

            add(BallItem(id = BallItem.Constants.BALL_ID_PREFIX + "17",
                    ImageIconRes(iconHq = R.drawable.ball_0017_hq, iconLq = R.drawable.ball_0017_lq),
                    status = ItemStates(isSelected = true, isPreUnlocked = true)))

           //add all other ball images here, as example given above
        }
    }
}