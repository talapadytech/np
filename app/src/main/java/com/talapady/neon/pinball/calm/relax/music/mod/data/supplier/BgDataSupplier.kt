package com.talapady.neon.pinball.calm.relax.music.mod.data.supplier

import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.menu.bg.BackgroundItem

object BgDataSupplier : GenericDataSupplier<BackgroundItem>() {
    override fun getSupply(): ArrayList<BackgroundItem> {

        return ArrayList<BackgroundItem>().apply {

            add(BackgroundItem(
                    id = BackgroundItem.Constants.BG_ID_PREFIX + "20",
                    icon = ImageIconRes(R.drawable.bg_0020_hq, R.drawable.bg_0020_lq),
                    tint = BackgroundItem.BgTint(100, 0, 0, 0),
                    status = ItemStates(isSelected = true, isPreUnlocked = true)
            ))

            //add all other bg images here, as example given above
        }
    }
}