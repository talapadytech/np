package com.talapady.neon.pinball.calm.relax.music.mod.tulu

import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil

class TuluSponsorModel(
    var list: ArrayList<SponsorItem?>? = null,
    var scroll: Scroll? = null,
    var splash: Splash? = null
)

class SponsorItem(
    var id: String? = null,
    var imageUrl: String? = null,
    var name: String? = null,
    var website: String? = null
) {
    fun isValid() = !GenericUtil.isAnyEmpty(id, imageUrl, name)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as SponsorItem
        if (id != other.id) return false
        return true
    }

    override fun hashCode() = id?.hashCode() ?: 0

}

class Scroll(shuffle: Boolean? = null, itemIds: ArrayList<String?>? = null) :
    OrderCommon(shuffle, itemIds)

class Splash(shuffle: Boolean? = null, itemIds: ArrayList<String?>? = null) :
    OrderCommon(shuffle, itemIds)

open class OrderCommon(var shuffle: Boolean? = null, var itemIds: ArrayList<String?>? = null)


