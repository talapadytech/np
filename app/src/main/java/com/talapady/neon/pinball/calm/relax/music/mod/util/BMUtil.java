package com.talapady.neon.pinball.calm.relax.music.mod.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

public class BMUtil {

    static public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {

        if (source == null || newHeight == 0 || newWidth == 0) {
            return null;
        }
        try {
            int sourceWidth = source.getWidth();
            int sourceHeight = source.getHeight();

            if (sourceWidth == 0 || sourceHeight == 0) {
                return null;
            }
            float xScale = (float) newWidth / sourceWidth;
            float yScale = (float) newHeight / sourceHeight;
            float scale = Math.max(xScale, yScale);

            // Now get the size of the source bitmap when scaled
            float scaledWidth = scale * sourceWidth;
            float scaledHeight = scale * sourceHeight;

            float left = (newWidth - scaledWidth) / 2;
            float top = (newHeight - scaledHeight) / 2;

            RectF targetRect = new RectF(left, top, left + scaledWidth, top
                    + scaledHeight);

            Bitmap dest = Bitmap.createBitmap(newWidth, newHeight,
                    source.getConfig());
            Canvas canvas = new Canvas(dest);
            canvas.drawBitmap(source, null, targetRect, null);
            return dest;
        } catch (Throwable t) {
            return null;
        } finally {
            //source.recycle();
        }
    }


    public static void recycle(Bitmap b) {
        if (b != null) {
            b.recycle();
        }
    }
}
