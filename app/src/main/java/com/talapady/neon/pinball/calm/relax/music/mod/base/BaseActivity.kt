package com.talapady.neon.pinball.calm.relax.music.mod.base

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.billing.BillingManager
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GameWideSettings
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallsActivity
import com.talapady.neon.pinball.calm.relax.music.mod.menu.offer.BuyAllActivity
import com.talapady.neon.pinball.calm.relax.music.mod.menu.settings.SettingsActivity
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import java.util.concurrent.TimeUnit


abstract class BaseActivity : AppCompatActivity() {

    private var mGenericHandler: Handler? = null

    private var firebaseAnalytics: FirebaseAnalytics? = null
    // private var fbAnalytics: AppEventsLogger? = null


    fun getHandler(): Handler {
        if (mGenericHandler == null) {
            mGenericHandler = Handler(Looper.getMainLooper())
        }
        return mGenericHandler as Handler
    }

    @Synchronized
    open fun resetScroll(rv: RecyclerView?) {
        if (rv == null) return

        runOnUiThread {
            try {
                rv.smoothScrollToPosition(0)
            } catch (t: Throwable) {
                ALog.e("resetScroll RecyclerView failed", t)
            }
        }
    }

    @Synchronized
    open fun resetScroll(sv: ScrollView?) {
        if (sv == null) return

        runOnUiThread {
            try {
                sv.fullScroll(ScrollView.FOCUS_UP)
            } catch (t: Throwable) {
                ALog.e("resetScroll ScrollView failed", t)
            }
        }
    }


    val navBarHideRunnable = {
        if (!ActivityUtil.isActivityFinishing(this@BaseActivity)) {
            hideSystemUI()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        backButtonBehaviour()
        bulkBuyBehaviour()
        settingsBehaviour()
        setUpCommonBg()
        changeBallBehaviour()
//        plusButtonBehaviour()
//        initAds()

        window?.decorView?.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                getHandler().removeCallbacks(navBarHideRunnable)
                getHandler().postDelayed(navBarHideRunnable, TimeUnit.SECONDS.toMillis(5))
            }
        }
    }



    protected fun backButtonBehaviour() =
        findViewById<View>(R.id.ivClose)?.setOnClickListener {
            logAnalyticsGeneric("closeButton", localClassName)
            finish()
        }

    protected fun settingsBehaviour() =
        findViewById<View>(R.id.settingsClickCapture)?.setOnClickListener {
            logAnalyticsGeneric("settingsButton", localClassName)
            launchActivity(SettingsActivity::class.java)
        }

    protected fun changeBallBehaviour() =
        findViewById<View>(R.id.mmChangeBall)?.setOnClickListener {
            logAnalyticsGeneric("ballChangeButton", localClassName)
            launchActivity(BallsActivity::class.java)
        }

    protected fun bulkBuyBehaviour() {
        val l = View.OnClickListener {
            logAnalyticsGeneric("bulkBuyButton", localClassName)
            launchActivity(BuyAllActivity::class.java)
        }

        findViewById<View>(R.id.bulkBuyClickCapture)?.setOnClickListener(l)
        findViewById<View>(R.id.bulkBuyClickCapture2)?.setOnClickListener(l) // few activities have two buttons leading to same activity
    }

    protected fun setUpCommonBg() = ImageUtil.setImageOomSafe(
        findViewById(R.id.ivCommonBgImage),
        R.drawable.common_bg_high,
        R.drawable.common_bg_low,
        localClassName
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        hideSystemUI()
        super.onCreate(savedInstanceState)
        try {
            ActivityUtil.hideKeyBoardOnLaunch(this)

            if (!BuildConfig.DEBUG) {
                firebaseAnalytics = FirebaseAnalytics.getInstance(this)
                //fbAnalytics = AppEventsLogger.newLogger(this)
            }
        } catch (t: Throwable) {
            ALog.e("Unable to create analytics", t)
        }
    }

    fun logAnalytics(event: String, params: Bundle) {
        try {
            if (!BuildConfig.DEBUG) {
                firebaseAnalytics?.logEvent(event, params)
                // fbAnalytics?.logEvent(event, params)
            }
        } catch (t: Throwable) {
            ALog.e("Unable to capture analytics", t)
        }
    }

    fun logAnalyticsGeneric(type: String?, item: String?) {
        val params = Bundle()
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, type)
        params.putString(FirebaseAnalytics.Param.ITEM_ID, item)
        logAnalytics(FirebaseAnalytics.Event.SELECT_CONTENT, params)
    }

    fun logPurchase(success: Boolean, itemId: String?) {
        val params = Bundle()
        if (GenericUtil.isNotEmpty(itemId)) params.putString("itemId", itemId)
        logAnalytics(
            if (success) "purchase_success_custom" else "purchase_failed_custom",
            params
        )
    }

    override fun onResume() {
        super.onResume()

        BillingManager.connectionRefresh(this)

        bulkBuyButtonVisibilityBehaviour()

    }

    protected open fun bulkBuyButtonVisibilityBehaviour() {
        ViewUtil.setVisibility(
            if (GameWideSettings.isFullGamePurchased()) View.GONE else View.VISIBLE,
            findViewById(R.id.bulkBuyClickCapture2),
            findViewById(R.id.bulkBuyClickCapture),
            findViewById(R.id.bulkBuyClickCaptureLine),
            findViewById(R.id.vLayoutGapAd)
        )
    }


    override fun onDestroy() {
        super.onDestroy()

    }

    public fun launchActivity(cls: Class<*>?) {
        startActivity(Intent(this, cls))
    }

    public fun launchActivity(cls: Class<*>?, code: Int) {
        startActivityForResult(Intent(this, cls), code)
    }

    public fun launchActivity(cls: Class<*>?, key: String, value: String) {
        startActivity(Intent(this, cls).putExtra(key, value))
    }

    public fun launchActivity(cls: Class<*>?, key: String, value: Boolean) {
        startActivity(Intent(this, cls).putExtra(key, value))
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }


    private fun hideSystemUI() {
        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                window.setDecorFitsSystemWindows(false)
//                window.insetsController?.let {
//                    it.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
//                    it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
//                }
//            } else {
            var flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // Hide the nav bar and status bar
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                flags = flags or View.SYSTEM_UI_FLAG_IMMERSIVE
            }
            window?.decorView?.systemUiVisibility = flags
//            }
        } catch (ignore: Throwable) {
        }
    }
}