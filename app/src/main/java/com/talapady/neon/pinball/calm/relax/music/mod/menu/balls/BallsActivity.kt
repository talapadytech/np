package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.android.billingclient.api.SkuDetails
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.ads.AdsManager
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.base.RewardBaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.billing.BillingManager
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.managers.BallImageManager
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.buy.BallDialog
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import kotlinx.android.synthetic.main.activity_balls.*
import java.util.*


class BallsActivity : RewardBaseActivity() {

    override fun getAdPlacementIdEnum() = AdsManager.AdPlacementIdEnum.AD_REWARD_BALL

    override fun rewardWatchedSuccess(itemId: String) {
        if (ActivityUtil.isActivityFinishing(this@BallsActivity)) return

        BallsManager.setTempUnlocked(itemId)
        ALog.toast(this, R.string.congrats_you_ve_unlocked_this_ball_for_free)
        //rvBalls?.adapter?.notifyDataSetChanged()
        handleSelectClick(ballItemReferenceViaAd)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_balls)

        val allBallData = BallsManager.getData()
        with(rvBalls) {
            adapter = BallsAdapter(allBallData, object : BallsAdapter.ItemClickListener {
                override fun onUnlockClick(position: Int?, data: ArrayList<BallItem>) = handleUnlockClick(position, data)
                override fun onSelectClick(ballItem: BallItem?) = handleSelectClick(ballItem)
            })
            layoutManager = GridLayoutManager(this@BallsActivity, 2)
            setHasFixedSize(true)
        }

        refreshPrices(allBallData)
    }


    private fun handleSelectClick(ballItem: BallItem?) {
        if (ballItem == null) {
            ALog.toast(this, R.string.unable_to_select_ball)
            logAnalyticsErrorStatic("handleSelectClick ballItem null")
            return
        }

        BallsManager.getSelectedItem()?.getStatus()?.isSelected = false
        BallsManager.setSelected(ballItem)
        BallImageManager.getInstance().invalidate()
        rvBalls?.adapter?.notifyDataSetChanged()

        logAnalyticsGeneric("ballSelected", ballItem.getId())
    }

    var ballItemReferenceViaAd: BallItem? = null
    private fun handleWatchAd(ballItem: BallItem?) {
        if (ballItem?.getId() == null) {
            ALog.e("Ball tag is null")
            ALog.toast(this@BallsActivity, R.string.purchase_failed)
            return
        }
        ballItemReferenceViaAd = ballItem
        super.showRewardVideo(ballItem.getId(), false)

        logAnalyticsGeneric("ballAdWatch", ballItem.getId())
    }


    private fun handleBuy(ballItem: BallItem?) {

        if (ballItem?.getId() == null) {
            ALog.e("Ball tag is null")
            ALog.toast(this@BallsActivity, R.string.purchase_failed)
            return
        }

        logAnalyticsGeneric("ballBuyClick", ballItem.getId())

        BillingManager.PurchaseFlow.startPurchaseFlow(this, ballItem.getId(), object : GenericCallBack<SkuDetails> {
            override fun done(success: Boolean, data: SkuDetails?) {
                BillingManager.PurchasedUtil.purchaseVerifyUiLoop(
                        this@BallsActivity,
                        ballItem,
                        success,
                        data,
                        R.string.enjoy_your_new_ball,
                        {
                            runOnUiThread {
                                handleSelectClick(ballItem)
                            }
                        }
                )
            }
        })
    }

    private fun refreshPrices(allBallData: ArrayList<BallItem>) {

        BillingManager.AllItems.getAllAvailableSkus(false, object : GenericCallBack<List<SkuDetails>> {
            override fun done(success: Boolean, data: List<SkuDetails>?) {

                if (ActivityUtil.isActivityFinishing(this@BallsActivity)) return

                var somethingChanged = false
                for (item in allBallData) {

                    val oldPriceItem = item.getUnlockWays().find { uw -> uw?.type == UnlockWay.UnlockType.BUY }
                            ?: continue

                    val found = data?.find { sku -> sku.sku == item.getId() }

                    if (found == null) {
                        ALog.e("Unable to find sku to display ball price. needed= ${item.getId()}")
                        continue
                    }

                    if (!GenericUtil.equals(oldPriceItem.price, found.price)) {
                        somethingChanged = true
                        oldPriceItem.price = found.price
                    }
                }

                if (somethingChanged) {
                    BallsManager.writeToDisk() //not strictly necessary
                    rvBalls?.adapter?.notifyDataSetChanged()
                }
            }
        })
    }


    private fun handleUnlockClick(position: Int?, data: ArrayList<BallItem>) {

        supportFragmentManager
                .setFragmentResultListener(BallDialog.Companion.Communicate.REQUEST_KEY, this) { requestKey, bundle ->
                    if (requestKey != BallDialog.Companion.Communicate.REQUEST_KEY) {
                        logAnalyticsErrorStatic("setFragmentResultListener invalid requestKey $requestKey")
                        return@setFragmentResultListener
                    }
                    val ballItem: BallItem? = bundle.getParcelable(BallDialog.Companion.Communicate.RESPONSE_DATA_KEY)
                    val action = bundle.getString(BallDialog.Companion.Communicate.Action.KEY)

                    if (ballItem == null || action == null) {
                        logAnalyticsErrorStatic("setFragmentResultListener ballItem $ballItem action $action")
                        return@setFragmentResultListener
                    }

                    when (action) {
                        BallDialog.Companion.Communicate.Action.VALUE_HANDLE_WATCH_AD -> handleWatchAd(ballItem)
                        BallDialog.Companion.Communicate.Action.VALUE_HANDLE_BUY -> handleBuy(ballItem)
                        else -> logAnalyticsErrorStatic("setFragmentResultListener invalid action $action")
                    }

                }

        val pos = if (position == null) {
            logAnalyticsErrorStatic("handleUnlockClick position null")
            0
        } else {
            position
        }


        val actualBall = GenericUtil.get(data, pos)
        val filteredList = ArrayList(data.filter { bi -> !bi.getStatus().isUnlocked() && UnlockWay.Util.hasAtleastOneValidUnlockWay(bi.getUnlockWays()) })
        var adjustedBallPosition = filteredList.indexOf(actualBall)

        adjustedBallPosition = if (adjustedBallPosition == -1) {
            logAnalyticsErrorStatic("handleUnlockClick adjustedBallPosition -1");
            0
        } else adjustedBallPosition

        BallDialog.newInstance(adjustedBallPosition, filteredList).show(supportFragmentManager, null)
    }

}
