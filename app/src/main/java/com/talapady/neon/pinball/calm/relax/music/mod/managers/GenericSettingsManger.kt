package com.talapady.neon.pinball.calm.relax.music.mod.managers

import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseModel
import com.talapady.neon.pinball.calm.relax.music.mod.util.SpManager

abstract class GenericSettingsManger<T> where T : GenericSettingsManger.GenericSettingItem {
    abstract class GenericSettingItem

    abstract fun getSaveKey(): String

    protected lateinit var data: T

    fun persist() {
        SpManager.getInstance().save(getSaveKey(), BaseModel.toJson(data))
    }

    fun read(clazz: Class<T>?): T? {
        val s = SpManager.getInstance().getString(getSaveKey())
        return BaseModel.fromJson(s, clazz)
    }


}

