package com.talapady.neon.pinball.calm.relax.music.mod.base

import android.os.Bundle
import android.view.View
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.ads.AdsManager
import com.talapady.neon.pinball.calm.relax.music.mod.ads.GenericRewardAdView
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timerTask

abstract class RewardBaseActivity : BaseActivity() {

    abstract fun getAdPlacementIdEnum(): AdsManager.AdPlacementIdEnum
    abstract fun rewardWatchedSuccess(itemId: String)
    protected open fun rewardWatchedSkipped(itemId: String) {

    }

    private var rewardItems: List<GenericRewardAdView>? = null


    var timer: Timer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createAndLoadRewardedAd()
    }

    private fun createAndLoadRewardedAd() {
        try {
            timer?.cancel()
            timer = Timer()
            isShowComplete = false
            rewardItems = AdsManager.loadReward(
                this,
                getAdPlacementIdEnum(),
                object : GenericRewardAdView.RewardResult {
                    override fun onClosed() {
                        getHandler().postDelayed({

                            logAdWatched(isShowComplete, itemId)

                            if (!isShowComplete) {
                                rewardWatchedSkipped(itemId)
                            } else {
                                rewardWatchedSuccess(itemId)
                            }
                            createAndLoadRewardedAd()
                        }, 500)
                    }

                    override fun onShowComplete() {
                        isShowComplete = true
                    }
                })

        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("AdditionalActivity createAndLoadRewardedAd ${ignore.message}")
        }
    }

    private lateinit var itemId: String
    private var isShowComplete: Boolean = false

    protected fun isAdReady(): Boolean {
        try {

            if (GenericUtil.isEmpty(rewardItems)) return false

            for (item in rewardItems!!) {
                if (item.isReady()) return true
            }

            return false
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("AdditionalActivity showRewardVideo ${ignore.message}")
            return false
        }
    }

    fun makeViewVisibleWhenAdIsReady(newRequest: Boolean = true, vararg views: View?) {


        try {
            if (newRequest) {
                timer?.cancel()
                timer = Timer()
            }
            if (ActivityUtil.isActivityFinishing(this)) return

            if (isAdReady()) {
                ViewUtil.setVisibility(View.VISIBLE, *views)
                timer?.cancel()
                return
            }

            timer?.schedule(
                timerTask { makeViewVisibleWhenAdIsReady(false, *views) },
                TimeUnit.SECONDS.toMillis(2)
            )
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("makeViewVisibleWhenAdIsReady ${ignore.message}")
        }
    }


    protected fun showRewardVideo(itemId: String, silent: Boolean = false): Boolean {
        try {
            this.itemId = itemId
            isShowComplete = false

            if (GenericUtil.isEmpty(rewardItems)) {
                if (!silent) {
                    ALog.toast(
                        this@RewardBaseActivity,
                        R.string.please_wait_ad
                    )
                }
                return false
            }

            var found = false
            for (item in rewardItems!!) {
                if (item.isReady()) {
                    found = true
                    item.show()
                    break
                }
            }

            if (!found) {
                if (!silent) {
                    ALog.toast(
                        this@RewardBaseActivity,
                        R.string.please_wait_ad
                    )
                }
                return false
            }

            return true
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("AdditionalActivity showRewardVideo ${ignore.message}")
            return false
        }
    }

    override fun onDestroy() {
        try {
            timer?.cancel()
            rewardItems?.let {
                for (item in it) {
                    item.onDestroy()
                }
            }
        } catch (ignore: Throwable) {
            logAnalyticsErrorStatic("AdditionalActivity onDestroy ${ignore.message}")
        }
        super.onDestroy()
    }

    fun logAdWatched(success: Boolean, itemId: String?) {
        val params = Bundle()
        if (GenericUtil.isNotEmpty(itemId)) params.putString("itemId", itemId)
        logAnalytics(
            if (success) "ad_watched_success_custom" else "ad_watched_skipped_custom",
            params
        )
    }
}
