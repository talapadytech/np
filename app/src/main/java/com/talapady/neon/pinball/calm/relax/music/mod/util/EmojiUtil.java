package com.talapady.neon.pinball.calm.relax.music.mod.util;

import androidx.core.text.HtmlCompat;

public class EmojiUtil {

    public static String getEmoji(String countryCode) {
        try {
            if (GenericUtil.isEmpty(countryCode) || GenericUtil.equalsRelaxed(countryCode, "-")) {
                return "";
            }
            int firstLetter = Character.codePointAt(countryCode, 0) - 0x41 + 0x1F1E6;
            int secondLetter = Character.codePointAt(countryCode, 1) - 0x41 + 0x1F1E6;
            return new String(Character.toChars(firstLetter)) + new String(Character.toChars(secondLetter));
        } catch (Throwable t) {
            ALog.e("getEmoji failed", t);
            return countryCode;
        }
    }

    public static String getCountryAndEmoji(String country, String countryCode) {
        return country + " " + getHtmlText(EmojiUtil.getEmoji(countryCode));
    }

    public static CharSequence getHtmlText(String string) {
        try {
            return HtmlCompat.fromHtml(string, HtmlCompat.FROM_HTML_MODE_LEGACY);
        } catch (Throwable t) {
            ALog.e("getCountryAndEmoji failed", t);
            return string;
        }
    }
}
