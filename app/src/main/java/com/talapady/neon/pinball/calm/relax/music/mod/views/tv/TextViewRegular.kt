package com.talapady.neon.pinball.calm.relax.music.mod.views.tv

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.res.ResourcesCompat
import com.talapady.neon.pinball.calm.relax.music.bouncy.R

class TextViewRegular : CommonTextView {
    private fun setCommonProperty() {
//        try {
//            // if (!SpManager.getInstance().getBoolean(SpManager.Font.IS_SYSTEM_FONT)) {
//            typeface = ResourcesCompat.getFont(context, R.font.averiasanslibre_bold)
//            //  }
//        } catch (ignore: Throwable) {
//        }
    }

    constructor(context: Context) : super(context) {
        setCommonProperty()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setCommonProperty()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    ) {
        setCommonProperty()
    }

}