package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.buy

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseFragment
import com.talapady.neon.pinball.calm.relax.music.mod.menu.balls.BallItem
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil


class BallsPagerAdapter(fm: FragmentManager, private var data: List<BallItem>) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): BaseFragment =
            BallPagerFragment.newInstance(GenericUtil.get(data, position).getIcon())

    override fun getCount() = GenericUtil.size(data)
}