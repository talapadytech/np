package com.talapady.neon.pinball.calm.relax.music.mod.views.popup

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.util.ImageUtil
import com.talapady.neon.pinball.calm.relax.music.mod.util.ViewUtil


class DecisionPopUp(var id: PopUpState, var c: Activity?) {
    init {
        c?.let {
            dialog = AlertDialog.Builder(it, R.style.TransparentDialog).setView(it.layoutInflater.inflate(R.layout.dialog_decision, null)).setCancelable(false).create()
            dialog?.setCanceledOnTouchOutside(false)
        }
    }

    var titleStringRes: Int? = null
    var positiveStringRes: Int? = null
    var dialog: AlertDialog? = null


    constructor(id: PopUpState,
                c: Activity?,
                titleStringRes: Int? = null,
                positiveTextRes: Int? = null) : this(id, c) {
        this.titleStringRes = titleStringRes
        this.positiveStringRes = positiveTextRes
    }


    interface ClickActions {
        fun positive()
        fun negative()
    }

    fun show(callback: ClickActions): AlertDialog? {

        val d = dialog ?: return null
        dialog?.show()

        ImageUtil.setBackgroundOomSafe(dialog?.findViewById(R.id.rlRootForBg), R.drawable.popup_bg, R.drawable.popup_bg_lq, javaClass.simpleName)
        val positiveButton: TextView? = d.findViewById(R.id.tvYes)
        val negativeButton: TextView? = d.findViewById(R.id.tvNo)

        titleStringRes?.let { titleStringRes -> d.findViewById<TextView?>(R.id.tvSure)?.setText(titleStringRes) }
        positiveStringRes?.let { positiveStringRes -> positiveButton?.setText(positiveStringRes) }

        positiveButton?.setOnClickListener {
            destroy(positiveButton, negativeButton)
            id.state = PopUpState.PopUp.INVALID
            callback.positive()
        }
        negativeButton?.setOnClickListener {
            destroy(positiveButton, negativeButton)
            id.state = PopUpState.PopUp.INVALID
            callback.negative()
        }

        return dialog
    }

    private fun destroy(positiveButton: View?, negativeButton: View?) {
        positiveButton?.setOnClickListener(null)
        negativeButton?.setOnClickListener(null)
        ViewUtil.dismissDialog(dialog)
    }
}

class PopUpState {
    enum class PopUp { RESTART, HOME, EXTRA_BALL, INVALID }

    var state = PopUp.INVALID

    fun setAndGet(s: PopUp): PopUpState {
        state = s; return this
    }
}