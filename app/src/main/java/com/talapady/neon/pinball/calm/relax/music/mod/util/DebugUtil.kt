package com.talapady.neon.pinball.calm.relax.music.mod.util

import android.content.Context
import android.os.AsyncTask
import android.provider.Settings
import com.facebook.ads.AdSettings
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import java.util.*


object DebugUtil {

    object GOOGLE {
        const val REWARD_TEST = "ca-app-pub-3940256099942544/5224354917"
    }

    fun setUpFbTestDevices(c: Context) {
        AsyncTask.execute {
            try {
                val adId = AdvertisingIdClient.getAdvertisingIdInfo(c)?.id
                if (GenericUtil.isEmpty(adId)) ALog.debug("failure to get adid") else AdSettings.addTestDevices(listOf(adId))
            } catch (e: Throwable) {
                ALog.debug("failure to get adid exception"); e.printStackTrace()
            }
        }
    }

    fun setUpGoogleTestDevices(c: Context) {

        MobileAds.setRequestConfiguration(
                RequestConfiguration.Builder().setTestDeviceIds(
                        listOf(
                                try {
                                    GenericUtil.md5(Settings.Secure.getString(c.contentResolver, Settings.Secure.ANDROID_ID)).toUpperCase(Locale.ROOT)
                                } catch (ignore: Throwable) {
                                    ALog.debug("setUpGoogleTestDevices failed $ignore")
                                    GenericUtil.EMPTY_STRING
                                }
                        )
                ).build()
        )
    }
}