package com.talapady.neon.pinball.calm.relax.music.mod.base

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.ScrollView
import androidx.recyclerview.widget.RecyclerView

interface CommonFragmentInterface {

    fun getActivityReference(): Activity?
    fun getHandler() = (getActivityReference() as? BaseActivity?)?.getHandler()
            ?: Handler(Looper.getMainLooper())

    fun logAnalytics(event: String, params: Bundle) {
        (getActivityReference() as? BaseActivity)?.logAnalytics(event, params)
    }

    fun logAnalyticsGeneric(type: String?, item: String?) {
        (getActivityReference() as? BaseActivity)?.logAnalyticsGeneric(type, item)
    }


    fun resetScroll(rv: RecyclerView?) {
        if (rv != null && getActivityReference() is BaseActivity) {
            (getActivityReference() as BaseActivity).resetScroll(rv)
        }
    }


    fun resetScroll(rv: ScrollView?) {
        if (rv != null && getActivityReference() is BaseActivity) {
            (getActivityReference() as BaseActivity).resetScroll(rv)
        }
    }

    fun onBackPressed(): Boolean {
        return false //false not handled
    }
}