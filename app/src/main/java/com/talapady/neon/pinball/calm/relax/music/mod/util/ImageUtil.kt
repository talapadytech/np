package com.talapady.neon.pinball.calm.relax.music.mod.util

import android.view.View
import android.widget.ImageView
import kotlin.reflect.KFunction1

object ImageUtil {

    fun setImageOomSafe(iv: ImageView?, hq: Int?, lq: Int?, className: String? = null) {
        if (iv == null) return
        setOomSafe(hq, lq, className, iv::setImageResource)
    }

    fun setBackgroundOomSafe(iv: View?, hq: Int?, lq: Int?, className: String? = null) {
        if (iv == null) return
        setOomSafe(hq, lq, className, iv::setBackgroundResource)
    }

    private fun setOomSafe(hq: Int?, lq: Int?, className: String? = null, setRes: KFunction1<Int, Unit>) {

        try {
            try {
                setRes(hq ?: lq ?: 0)
            } catch (t: OutOfMemoryError) {
                logAnalyticsErrorStatic("OOM: $className")
                setRes(lq ?: hq ?: 0)
            }
        } catch (ignore: Throwable) {
            ignore.printStackTrace()
        }
    }
}