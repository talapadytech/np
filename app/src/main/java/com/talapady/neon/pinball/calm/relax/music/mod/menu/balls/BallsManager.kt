package com.talapady.neon.pinball.calm.relax.music.mod.menu.balls

import com.talapady.neon.pinball.calm.relax.music.mod.data.supplier.BallsDataSupplier
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager

object BallsManager : GenericManager<BallItem>() {
    override fun getSaveKey() = "BallsManager"
    override fun getStaticData() = BallsDataSupplier.getSupply()
    override fun getGenericsHack() = BallItem::class.java
}