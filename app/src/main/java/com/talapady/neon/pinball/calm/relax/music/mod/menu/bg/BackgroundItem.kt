package com.talapady.neon.pinball.calm.relax.music.mod.menu.bg

import android.os.Parcelable
import com.talapady.neon.pinball.calm.relax.music.mod.generic.Bonus
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ImageIconRes
import com.talapady.neon.pinball.calm.relax.music.mod.generic.ItemStates
import com.talapady.neon.pinball.calm.relax.music.mod.generic.UnlockWay
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GenericManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil
import kotlinx.android.parcel.Parcelize


@Parcelize
data class BackgroundItem(private var id: String = GenericUtil.EMPTY_STRING,
                          private var icon: ImageIconRes = ImageIconRes(),
                          private var status: ItemStates = ItemStates(),
                          private var bonus: ArrayList<Bonus?> = ArrayList(),
                          private var unlockWays: ArrayList<UnlockWay?> = ArrayList(),
                          private var tint: BgTint = BgTint(apply = false))
    : GenericManager.GenericItem(), Parcelable {

    @Parcelize
    data class BgTint(val apply: Boolean = false,
                      var a: Int = GenericUtil.ZERO,
                      var r: Int = GenericUtil.ZERO,
                      var g: Int = GenericUtil.ZERO,
                      var b: Int = GenericUtil.ZERO) : Parcelable {

        constructor(a: Int, r: Int, g: Int, b: Int) : this(true, a, r, g, b)
    }

    object Constants {
        const val BG_ID_PREFIX = "bg_"
    }

    override fun getId() = id
    override fun getIcon() = icon
    override fun getStatus() = status
    override fun getBonus() = bonus
    override fun getUnlockWays() = unlockWays
    fun getTint() = tint
    override fun getIdPrefix() = Constants.BG_ID_PREFIX
}