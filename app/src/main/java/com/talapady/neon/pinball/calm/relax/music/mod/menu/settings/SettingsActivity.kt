package com.talapady.neon.pinball.calm.relax.music.mod.menu.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.core.view.children
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import com.talapady.neon.pinball.calm.relax.music.bouncy.R
import com.talapady.neon.pinball.calm.relax.music.mod.base.BaseActivity
import com.talapady.neon.pinball.calm.relax.music.mod.base.GenericCallBack
import com.talapady.neon.pinball.calm.relax.music.mod.data.config.ConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.data.config.ConfigModel
import com.talapady.neon.pinball.calm.relax.music.mod.locale.LocaleManager
import com.talapady.neon.pinball.calm.relax.music.mod.managers.GameWideSettings
import com.talapady.neon.pinball.calm.relax.music.mod.managers.SoundManager
import com.talapady.neon.pinball.calm.relax.music.mod.os.OpenSourceLicencesActivity
import com.talapady.neon.pinball.calm.relax.music.mod.tulu.TuluSponsorManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.*
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity() {
    object CONSTANTS {
        const val openLang = "openLang"
    }

    private var mConfigData: ConfigModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        //<audio>
        SoundManager.ViewUtil.refreshSoundViews(soundIcon, soundText)
        SoundManager.ViewUtil.refreshMusicViews(musicIcon, musicText)

        soundToggleContainer.setOnClickListener {
            val action = SoundManager.toggleSound(soundIcon, soundText); logAnalyticsGeneric(
            "sound$action",
            localClassName
        )
        }
        musicToggleContainer.setOnClickListener {
            val action = SoundManager.toggleMusic(musicIcon, musicText); logAnalyticsGeneric(
            "music$action",
            localClassName
        )
        }
        //</audio>

        //<checkbox>
        ViewUtil.associateCheckBox(cbIndependentFlippers, cbIndependentFlippersText)
        ViewUtil.associateCheckBox(cbZoomBall, cbZoomBallText)
        ViewUtil.associateCheckBox(cbHapticFeedback, cbHapticFeedbackText)

        cbIndependentFlippers.isChecked = GameWideSettings.isIndependentFlippersOn()
        cbZoomBall.isChecked = GameWideSettings.isZoomOn()
        cbHapticFeedback.isChecked = GameWideSettings.isHapticOn()

        cbIndependentFlippers.setOnCheckedChangeListener { _, _ ->
            GameWideSettings.toggleIndependentFlippers(cbIndependentFlippers)
            logAnalyticsGeneric(
                "cbIndependentFlippers",
                cbIndependentFlippers.isChecked.toString() + "-" + localClassName
            )
        }
        cbZoomBall.setOnCheckedChangeListener { _, _ ->
            GameWideSettings.toggleZoom(cbZoomBall)
            logAnalyticsGeneric(
                "cbZoomBall",
                cbZoomBall.isChecked.toString() + "-" + localClassName
            )
        }
        cbHapticFeedback.setOnCheckedChangeListener { _, isChecked ->
            GameWideSettings.setHaptic(isChecked)
            logAnalyticsGeneric("cbHapticFeedback", "$isChecked-$localClassName")
        }
        //<checkbox>

        clickHandle()

        handleTuluSponsor()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        if (intent?.extras?.getBoolean(CONSTANTS.openLang) == true) {
            showLanguageView()
        }
    }

    private fun handleTuluSponsor() {
        ViewUtil.setVisibility(View.GONE, hsvSponsor, hsvStaticText)

        val tuluSponsors = TuluSponsorManager.getListForSettingScreen()
        if (!LocaleManager.isTulu() || GenericUtil.isEmpty(tuluSponsors)) return

        ViewUtil.setVisibility(View.VISIBLE, hsvSponsor, hsvStaticText)

        tuluSponsors?.let {
            llForHsv.removeAllViews()
            for (item in it) {

                if (item?.isValid() == false) continue

                val view: LinearLayoutCompat = LayoutInflater.from(this)
                    .inflate(R.layout.tulu_hsv_root_ll, llForHsv, false) as? LinearLayoutCompat
                    ?: continue

                llForHsv.addView(view)

                Glide.with(this).load(item?.imageUrl)
                    .into(view.findViewById<AppCompatImageView>(R.id.ivSponsor))
                view.findViewById<AppCompatTextView>(R.id.tvSponsor).text = item?.name

                view.setOnClickListener {
                    ActivityUtil.openBrowser(this, item?.website)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ConfigManager.getData(object : GenericCallBack<ConfigModel> {
            override fun done(success: Boolean, data: ConfigModel?) {
                if (success && data != null) {
                    mConfigData = data
                }
            }
        })
    }

    private fun clickHandle() {

        llRate.setOnClickListener {
            logAnalyticGenericSettings("Rate")
            ActivityUtil.openPlayStore(this, ActivityUtil.getMyPackageName(this))
        }

        llShare.setOnClickListener {
            logAnalyticGenericSettings("Share")

            if (mConfigData == null || GenericUtil.isEmpty(mConfigData?.shareUrl)) {
                ActivityUtil.shareMyApp(
                    this,
                    ActivityUtil.PLAY_STORE_URL + ActivityUtil.getMyPackageName(this)
                )
            } else {
                ActivityUtil.shareMyApp(this, mConfigData?.shareUrl)
            }
        }

        llEmail.setOnClickListener {
            logAnalyticGenericSettings("Email")
            if (mConfigData == null || GenericUtil.isEmpty(mConfigData?.feedbackEmail)) {
                logAnalyticsErrorStatic(ALog.toast(this, R.string.config_data_not_found))
            } else {
                ActivityUtil.openSupportEmail(this, mConfigData?.feedbackEmail)
            }
        }

        llPrivacy.setOnClickListener {
            logAnalyticGenericSettings("Privacy")
            if (mConfigData == null || GenericUtil.isEmpty(mConfigData?.privacyUrl)) {
                logAnalyticsErrorStatic(ALog.toast(this, R.string.config_data_not_found))
            } else {
                ActivityUtil.openBrowser(this, mConfigData?.privacyUrl)
            }
        }
        llTerms.setOnClickListener {
            logAnalyticGenericSettings("Terms")
            if (mConfigData == null || GenericUtil.isEmpty(mConfigData?.termsUrl)) {
                logAnalyticsErrorStatic(ALog.toast(this, R.string.config_data_not_found))
            } else {
                ActivityUtil.openBrowser(this, mConfigData?.termsUrl)
            }
        }

        llSocial.setOnClickListener {
            logAnalyticGenericSettings("Social")
            if (mConfigData == null || GenericUtil.isEmpty(mConfigData?.socialMainUrl)) {
                logAnalyticsErrorStatic(ALog.toast(this, R.string.config_data_not_found))
            } else {
                ActivityUtil.openBrowser(this, mConfigData?.socialMainUrl)
            }
        }

        llOpenSource.setOnClickListener {
            logAnalyticGenericSettings("OpenSource")
            launchActivity(OpenSourceLicencesActivity::class.java)
        }

        llLanguage.setOnClickListener {
            logAnalyticGenericSettings("Language")
            showLanguageView()
        }

        vLanguageFade.setOnClickListener {
            logAnalyticGenericSettings("vLanguageFade")
            hideLanguageView()
        }

        llLanguageRoot.setOnClickListener {
            //to avoid click transfer to views in the back
        }
        for (child in llLanguageRoot.children) {
            child.tag as? String ?: continue

            if (child is TextView) {
                child.setTextColor(
                    ContextCompat.getColor(
                        this,
                        if (GenericUtil.equals(
                                SpManager.getInstance().locale,
                                child.tag as? String
                            )
                        ) R.color.primaryPink else R.color.primaryYellow
                    )
                )
            }

            child.setOnClickListener {
                val tagRunTime = it.tag as? String

                if (tagRunTime != null) {
                    logAnalyticsSelectLanguage(tagRunTime)
                    LocaleManager.setLanguage(this@SettingsActivity, tagRunTime)
                } else {
                    logAnalyticsSelectLanguageFailed()
                    ALog.e(
                        ALog.toast(
                            it.context,
                            getString(
                                R.string.unable_to_change_language,
                                getString(R.string.please_report_to_dev)
                            )
                        )
                    )
                }
                hideLanguageView()
            }
        }

    }

    override fun onBackPressed() {
        if (llLanguageRootScroll?.visibility == View.VISIBLE) {
            hideLanguageView()
        } else {
            super.onBackPressed()
        }
    }

    private fun showLanguageView() =
        ViewUtil.setVisibility(View.VISIBLE, llLanguageRootScroll, vLanguageFade)

    private fun hideLanguageView() =
        ViewUtil.setVisibility(View.GONE, llLanguageRootScroll, vLanguageFade)

    private fun logAnalyticsSelectLanguage(languageCode: String?) {
        val params = Bundle()
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "languageSelection")
        params.putString(FirebaseAnalytics.Param.ITEM_ID, languageCode)
        logAnalytics(FirebaseAnalytics.Event.SELECT_CONTENT, params)
    }

    private fun logAnalyticsSelectLanguageFailed() {
        val params = Bundle()
        params.putString("errorMessage", "languageSelection failed, tag null")
        logAnalytics("GeneralError", params)
    }

    private fun logAnalyticGenericSettings(item: String?) {
        logAnalyticsGeneric("settings", item)
    }
}