package com.talapady.neon.pinball.calm.relax.music.vectorpinball.model;

import java.util.Timer;
import java.util.TimerTask;

public class GameState {

    // Defines how score multiplier is affected when ball is lost.
    public enum MultiplierBehavior {
        REMOVE,  // Reset to 1.
        HOLD,    // Don't change.
        ROUND_HALF_DOWN,  // Reduce by half and round down, the default.
    }

    boolean gameInProgress = false; //made true once when game starts, made false, at the end of game only once
    boolean paused = true; //made true, false multiple times during the gameplay

    public boolean isGameStarted() {
        return gameStarted;
    }

    boolean gameStarted = false; //made true once when game starts, not made false, at the end of game

    int ballNumber;
    int extraBalls;
    int totalBalls = 3;
    boolean unlimitedBalls;

    long score;
    double scoreMultiplier;
    MultiplierBehavior multiplierBehavior;

    public void startNewGame() {
        score = 0;
        ballNumber = 1;
        scoreMultiplier = 1;
        multiplierBehavior = MultiplierBehavior.ROUND_HALF_DOWN;

        gameStarted = true;
        gameInProgress = true;
        paused = false;

        initTimer();
    }

    public void doNextBall() {
        switch (multiplierBehavior) {
            case REMOVE:
                scoreMultiplier = 1;
                break;
            case HOLD:
                break;
            case ROUND_HALF_DOWN:
                scoreMultiplier = Math.max(1, Math.floor(scoreMultiplier / 2));
                break;
        }

        if (extraBalls > 0) {
            --extraBalls;
        } else if (unlimitedBalls || ballNumber < totalBalls) {
            ++ballNumber;
        } else {
            gameInProgress = false;
        }
    }

    public void addScore(long points) {
        score += points * getScoreMultiplier();
    }

    public void addExtraBall() {
        ++extraBalls;
    }

    public void incrementScoreMultiplier() {
        scoreMultiplier += 1;
    }

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public void setGameInProgress(boolean value) {
        gameInProgress = value;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean value) {
        paused = value;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public int getExtraBalls() {
        return extraBalls;
    }

    public void setExtraBalls(int extraBalls) {
        this.extraBalls = extraBalls;
    }

    public int getTotalBalls() {
        return totalBalls;
    }

    public void setTotalBalls(int totalBalls) {
        this.totalBalls = totalBalls;
    }

    public boolean hasUnlimitedBalls() {
        return unlimitedBalls;
    }

    public void setUnlimitedBalls(boolean unlimited) {
        this.unlimitedBalls = unlimited;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public double getScoreMultiplier() {
        return scoreMultiplier + adMultiplier + totalBonusMultiplier;
    }

    public void setScoreMultiplier(double scoreMultiplier) {
        this.scoreMultiplier = scoreMultiplier;
    }

    public MultiplierBehavior getMultiplierBehavior() {
        return multiplierBehavior;
    }

    public void setMultiplierBehavior(MultiplierBehavior behavior) {
        multiplierBehavior = behavior;
    }


    long time;
    Timer timer;

    public void setFieldActive(boolean fieldActive) {
        isFieldActive = fieldActive;
    }

    boolean isFieldActive = false;


    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    private void initTimer() {
        cancelTimer();
        time = 0;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isGameInProgress()) {
                    if (!isPaused() && isFieldActive) {
                        time++;

                        if (adMultiplier != 0 && time > adMultiplierDuration) {
                            adMultiplier = 0;
                        }
                    }
                } else {
                    //cancelTimer();
                }
            }
        }, 0, 1000);
    }

    public long getTime() {
        return time;
    }

    private float adMultiplier = 0;
    private int adMultiplierDuration = 0;
    private float totalBonusMultiplier = 0;

    public void setAdMultiplier(float adMultiplier, int adMultiplierDuration) {
        this.adMultiplier = adMultiplier;
        this.adMultiplierDuration = adMultiplierDuration;
    }

    public void setTotalBonusMultiplier(float totalBonusMultiplier) {
        this.totalBonusMultiplier = totalBonusMultiplier;
    }
}
