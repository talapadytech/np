package com.talapady.neon.pinball.calm.relax.music.mod.base

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.ads.AudienceNetworkAds
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.talapady.neon.pinball.calm.relax.music.bouncy.BuildConfig
import com.talapady.neon.pinball.calm.relax.music.mod.billing.BillingManager
import com.talapady.neon.pinball.calm.relax.music.mod.data.config.ConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.locale.LocaleManager
import com.talapady.neon.pinball.calm.relax.music.mod.remoteconfig.RemoteConfigManager
import com.talapady.neon.pinball.calm.relax.music.mod.update.UpdateManager
import com.talapady.neon.pinball.calm.relax.music.mod.util.*


class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initComponents(this)
        initAds(this)
        initNotification()
    }


    private fun initAds(context: Application) {
        try {
            MobileAds.initialize(context) {}
            if (!AudienceNetworkAds.isInitialized(context)) {
                AudienceNetworkAds.initialize(context)
            }

            if (BuildConfig.DEBUG) {
                DebugUtil.setUpGoogleTestDevices(this)
                DebugUtil.setUpFbTestDevices(this)
            }
        } catch (t: Throwable) {
            ALog.e("initAds failed", t)
        }
    }

    //
    companion object {
        @JvmStatic
        fun initComponents(c: Application?, runSchedulers: Boolean = true) {
            try {
                if (c == null) {
                    ALog.e("initComponents received null context")
                    return
                }
                SpManager.getInstance().init(c)
                BillingManager.connectionRefresh(c)
                RemoteConfigManager.init()
                ConfigManager.getData(null)

                ViewUtil.computeCommonParams(c)
                UpdateManager.init(c)

                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

                FontUtil.init(c)
                LocaleManager.init(c)
            } catch (t: Throwable) {
                ALog.e("initComponents failed", t)
            }
        }
    }


    private fun initNotification() {
        if (BuildConfig.DEBUG) {
            FirebaseMessaging.getInstance().token
                    .addOnCompleteListener(object : OnCompleteListener<String?> {
                        override fun onComplete(task: Task<String?>) {
                            if (!task.isSuccessful) {
                                ALog.debug("Fetching FCM registration token failed" + task.exception)
                                return
                            }
                            val token: String? = task.result
                            ALog.debug("FCM token $token")
                        }
                    }).addOnFailureListener { e -> ALog.debug("Fetching FCM registration token failed addOnFailureListener $e") }
        }
    }
}