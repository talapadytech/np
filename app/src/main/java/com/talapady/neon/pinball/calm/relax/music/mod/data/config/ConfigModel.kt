package com.talapady.neon.pinball.calm.relax.music.mod.data.config


data class ConfigModel(
    var version: String? = null,
    var forceUpdate: List<String?>? = null,
//    var refreshFreq: Long? = 24*7,

    var feedbackEmail: String? = null,
    var privacyUrl: String? = null,
    var termsUrl: String? = null,
    var socialMainUrl: String? = null,
    var shareUrl: String? = null,

)
