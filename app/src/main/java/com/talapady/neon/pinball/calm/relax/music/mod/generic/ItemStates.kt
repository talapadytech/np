package com.talapady.neon.pinball.calm.relax.music.mod.generic

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemStates(var isSelected: Boolean = false,
                      var isPreUnlocked: Boolean = false,
                      var isPurchased: Boolean = false,
                      var isScoreUnlocked: Boolean = false,
                      @Transient var isTempUnlocked: Boolean = false) : Parcelable {

    fun isUnlocked() = isPreUnlocked || isPurchased || isScoreUnlocked || isTempUnlocked
}


