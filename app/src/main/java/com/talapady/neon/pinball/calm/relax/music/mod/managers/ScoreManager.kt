package com.talapady.neon.pinball.calm.relax.music.mod.managers

import com.talapady.neon.pinball.calm.relax.music.mod.util.GenericUtil

object ScoreManager : GenericSettingsManger<ScoreManager.ScoreData>() {
    override fun getSaveKey() = "ScoreManager" //don't use getClassName, might get stripped during proguard
    class ScoreData(var formatVersion: Int = 1, var scores: HashMap<String, Score> = HashMap()) : GenericSettingItem() {
        class Score(var id: String = GenericUtil.EMPTY_STRING, var highScore: Long = 0, var highTime: Long = 0, var lastScore: Long = 0, var lastTime: Long = 0)
    }

    init {
        data = read(ScoreData::class.java) ?: ScoreData()
    }


    fun setScoreAndTime(id: String, lastScore: Long, lastTime: Long) {
        var score = data.scores[id]
        if (score == null) {
            score = ScoreData.Score()
            data.scores[id] = score
        }

        score.lastScore = lastScore
        score.lastTime = lastTime

        if (score.highScore < score.lastScore) {
            score.highScore = score.lastScore
        }
        if (score.highTime < score.lastTime) {
            score.highTime = score.lastTime
        }

        persist()
    }

    fun get(id: String): ScoreData.Score {
        var score = data.scores[id]
        if (score == null) {
            score = ScoreData.Score()
            data.scores[id] = score
            persist()
        }
        return score
    }

    fun getTopHighScore() = data.scores.maxOfOrNull { it.value.highScore } ?: 0
    fun getTopHighTime() = data.scores.maxOfOrNull { it.value.highTime } ?: 0

}