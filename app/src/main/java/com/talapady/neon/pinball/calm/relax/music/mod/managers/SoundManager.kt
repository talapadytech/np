package com.talapady.neon.pinball.calm.relax.music.mod.managers

import android.content.res.ColorStateList
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.talapady.neon.pinball.calm.relax.music.bouncy.R

object SoundManager : GenericSettingsManger<SoundManager.SoundData>() {
    override fun getSaveKey() = "SoundManager" //don't use getClassName, might get stripped during proguard
    class SoundData(var isSoundOn: Boolean = true, var isMusicOn: Boolean = true) : GenericSettingItem()

    init {
        data = read(SoundData::class.java) ?: SoundData()
    }

    fun isSoundOn(): Boolean = data.isSoundOn
    fun isMusicOn(): Boolean = data.isMusicOn

    private fun setSoundState(isOn: Boolean) {
        data.isSoundOn = isOn
        persist()
    }

    private fun setMusicState(isOn: Boolean) {
        data.isMusicOn = isOn
        persist()
    }

    fun toggleSound(soundIcon: ImageView? = null, soundText: TextView? = null): String {
        val action = !isSoundOn()
        setSoundState(action)
        ViewUtil.refreshSoundViews(soundIcon, soundText)
        return if(action) "On" else "Off"
    }

    fun toggleMusic(musicIcon: ImageView? = null, musicText: TextView? = null): String {
        val action = !isMusicOn()
        setMusicState(action)
        ViewUtil.refreshMusicViews(musicIcon, musicText)
        return if(action) "On" else "Off"
    }

    object ViewUtil {

        fun refreshSoundViews(iv: ImageView?, tv: TextView? = null) {
            refreshViews(iv, tv, ::isSoundOn)
        }

        fun refreshMusicViews(iv: ImageView?, tv: TextView? = null) {
            refreshViews(iv, tv, ::isMusicOn)
        }

        private fun refreshViews(iv: ImageView?, tv: TextView? = null, soundOrMusicOn: () -> Boolean) {

            val tintColor = getTintColor(soundOrMusicOn())

            if (iv != null) {
                ImageViewCompat.setImageTintList(iv, ColorStateList.valueOf(ContextCompat.getColor(iv.context, tintColor)))
            }



            tv?.let {

                val textRes = if (soundOrMusicOn == ::isSoundOn) {
                    if (isSoundOn()) R.string.sound_is_on else R.string.sound_is_off
                } else if (soundOrMusicOn == ::isMusicOn) {
                    if (isMusicOn()) R.string.music_is_on else R.string.music_is_off
                } else {
                    R.string.empty_string
                }

                it.setText(textRes)
                it.setTextColor(ContextCompat.getColor(tv.context, tintColor))
            }

        }

        private fun getTintColor(enabled: Boolean) =
                if (enabled) R.color.primaryYellow else R.color.disabled_tint


    }
}