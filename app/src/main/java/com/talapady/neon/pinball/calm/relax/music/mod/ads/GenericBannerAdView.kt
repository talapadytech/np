package com.talapady.neon.pinball.calm.relax.music.mod.ads

interface GenericBannerAdView {
    fun onDestroy()
}


interface GenericRewardAdView {
    fun isReady():Boolean
    fun show()
    fun onDestroy()

    interface RewardResult{
        fun onClosed()
        fun onShowComplete()
    }
}